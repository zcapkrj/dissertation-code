# chips-kai

Code from Kai's research project

There will be two files (darkroom and cosmics):

darkroom holds my macros for the darkroom analysis - still have some to add and 
current one macro isn't that I need to fix

cosmics has macros for analysis of the cosmic data, is split into a few files
- datafiles: where to store the files going to analysed and where any new files created by programs go 
- filtering: files that filter the data files or do checks on them
- clustering: macros that perform the clusting of signals