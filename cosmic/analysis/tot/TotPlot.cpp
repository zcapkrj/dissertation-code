/*
for a file plots the tot of every signal
note:when going above max time of 2s need to increase bin sizes 
*/

#include "../../CoreFuns.cpp"

void PlotSignalsTot(TString fileName, TString title, Double_t maxTime, Double_t focus, TCanvas *c);


void TotPlot() {

  //TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  //TString fileName = "../../datafiles/FilteredForTot.root";
  TString fileName = "../../datafiles/FilteredJustForTot.root";
  

  TCanvas *c = new TCanvas("c", "c", 1500, 300);
  //c->SetBottomMargin(0.3);
  //c->Divide(1,2,0.01,0.01,0); // divide canvas ( set for 30 channels)

  PlotSignalsTot(fileName, "ToT vs TimeStamp for 1 ms of filtered data", 2,1, c);
  
  //c->cd(1);
  //PlotSignalsTot(fileName, "ToT vs TimeStamp of the filtered data (>30ns ToT)", 1,0, c);
  
  //c->cd(2);
  //PlotSignalsTot(fileName, "ToT between 1 and 1.001 s", 1, 0, c); 

}


void PlotSignalsTot(TString fileName, TString title, Double_t maxTime, Double_t focus, TCanvas *c) {
  std::cout << std::setprecision(15) << std::endl;
  c->SetLeftMargin(0.9);
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // init files for loop 
  Double_t signalTime = 0;
  Double_t int_tot;
  Double_t int_time;
  Double_t maxTime_ns = maxTime * s_To_ns; // convert to ns

  UInt_t lastSignalIndex = signalTree->GetEntries(); std::cout << lastSignalIndex << std::endl;

  TH1F *histo = new TH1F("histo", "Event Signals", maxTime_ns, 0, maxTime_ns);
  histo->SetTitle(title +"; Time/s; ToT/ns");

  UInt_t i = 0; UInt_t count = 0;
  while (signalTime < maxTime_ns) {
    //for (int i = 0; i < lastSignalIndex; i++) {
    
    signalTree->GetEntry(i); 
    signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

    int_time = (UInt_t) (signalTime); // when going above max time of 2 need to increase bin sizes 
    int_tot  = (int) tot;

    if (int_tot > 0) {
      histo->AddBinContent(int_time, int_tot); // since we are using bin content if in the same bin will be written 
      count++;
    }
    i++;
  }

  if (focus != 999) { // an out so can look at whole thing
    
    histo->GetXaxis()->SetRangeUser(focus * s_To_ns, (focus+0.0011) * s_To_ns);
  }

  std::cout << count << std::endl;

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.08);
  gStyle->SetLabelSize(0.07, "XY");
  gStyle->SetTitleSize(0.08,"XY");
  gStyle->SetTitleOffset(.5,"X");
  gStyle->SetTitleOffset(.3,"Y");  
  
  histo->Draw();
  histo->UseCurrentStyle();
  histo->GetXaxis()->SetLimits(0, maxTime);
  histo->GetYaxis()->SetLimits(0, 200);
}
