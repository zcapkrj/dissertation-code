#include "../../CoreFuns.cpp"

void GetPMTx(float & x, int pmtPos, int _phx, int _phy);
void GetPMTy (float & y, int _pomType, int pmtPos, int _phx, int _phy);
void getXYcoords(std::string line, int channel, float & x, float & y);
bool IsBeside(Signal signal1, Signal signal2);
void AddToPmtPosFreqPlot (TH1F *h, Signal signal);
void GetPmtPos(std::string line, UInt_t channel, UInt_t & pmtPos);


// checks if two signals are beside each other
bool IsBeside(Signal signal1, Signal signal2) {
  
  TString id1 = ToString(signal1.pomId); // convert pomId to string
  TString id2 = ToString(signal2.pomId); // convert pomId to string
  float x1; float x2; float y1; float y2;
  
  ifstream myfile ("../../datafiles/pomPmtPositionMap.txt"); // open config file
  std::string line; // varible to hold each line
    
  if (myfile.is_open()) { // if file was opened

    while ( getline(myfile, line) ) { // read every line of file 

      if ( line.find(id1) != std::string::npos) { // check if right pomid

	getXYcoords(line, signal1.channel, x1, y1);
      }

      if ( line.find(id2) != std::string::npos) { // check if right pomid

	getXYcoords(line, signal2.channel, x2, y2);
      }
    }
    myfile.close();

    if ( std::abs(x1-x2) == 1 || std::abs(y1-y2) ) {
      return true;
    }
    else {
      return false;
    }
  }
  
  return false;
}


void getXYcoords(std::string line, int channel, float & x, float & y) {
  
  std::istringstream s(line); // stream for line
  std::vector<string> elements; // variable to hold each element of line
  string element; // hold each element of line 
  
  while (s >> element) {
    elements.push_back(element); // add element to end of elements vector
  }

  int type_index           = std::stoi(elements[1]);
  int pom_x_position_index = std::stoi(elements[2]);
  int pom_y_position_index = std::stoi(elements[3]);
  int pmtPos               = std::stoi(elements[5 + channel]);
  
  GetPMTx(x, pmtPos, pom_x_position_index, pom_y_position_index);
  GetPMTy(y, type_index, pmtPos, pom_x_position_index, pom_y_position_index);
}


void  GetPMTx (float & x, int pmtPos, int _phx, int _phy) {

  x = -999;

  if (pmtPos >= 0) {
    x = -((_phx-1) * 5 +  ( (pmtPos - 1) % 5) + 1);
  }
}
 

void GetPMTy (float & y, int _pomType, int pmtPos, int _phx, int _phy) {
  
  y = -999;
  
  if(_pomType == 0 && pmtPos > 0) {
    y = ( 5*(8 - _phy-0.6 * abs(_phx-1)) -  ((pmtPos-1) / 5) );
  }
  else if(_pomType == 2 ) {
    y = (   6*( 7.8 - _phy -0.6 * abs(_phx-1) ) -  ((pmtPos-1)/5));  // 7.8 not correct 
  }
}


// get a signal find the channel pos and plot on a frequency plot
void AddToPmtPosFreqPlot (TH1F *h, Signal signal, std::vector<UInt_t> bcPlanes) {

  TString pomIdString = ToString(signal.pomId); // convert pomId to string
  UInt_t pmtPos; UInt_t bcPlaneNum; 
  
  ifstream myfile ("../../datafiles/pomPmtPositionMap.txt"); // open config file
  std::string line; // varible to hold each line
    
  if (myfile.is_open()) { // if file was opened
    while ( getline(myfile, line) ) { // read every line of file 

      if ( line.find(pomIdString) != std::string::npos) { // check if right pomid
	
	GetPmtPos(line, signal.channel, pmtPos);

	// use it to find location of bc plane of signal
	std::vector<UInt_t>::iterator it = std::find(bcPlanes.begin(), bcPlanes.end(), signal.pomId);
	bcPlaneNum = std::distance(bcPlanes.begin(), it); // get index of plane

	//std::cout << bcPlanes.begin() << std::endl;
	//std::cout << bcPlaneNum << " " << signal.pomId << " " << " " << signal.channel << " " << pmtPos << std::endl;
	
	h->Fill((pmtPos) + (30 * bcPlaneNum)); // 30 as assuming all have 30 channels
      }
    }
  }
  
  myfile.close();
}


// get pmt pos from a line in the convig fill
void GetPmtPos(std::string line, UInt_t channel, UInt_t & pmtPos) {
  
  std::istringstream s(line); // stream for line
  std::vector<string> elements; // variable to hold each element of line
  string element; // hold each element of line 
  
  while (s >> element) {
    elements.push_back(element); // add element to end of elements vector
  }
  
  pmtPos = std::stoi(elements[5 + channel]);  
}

