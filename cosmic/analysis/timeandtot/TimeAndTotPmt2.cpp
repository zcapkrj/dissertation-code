/*
filter out the two pmts that have lots of noise
*/

#include "../../CoreFuns.cpp"

void TimeAndTotPmt2Plot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq, UInt_t maxTime);


void TimeAndTotPmt2() {
  //TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "../../datafiles/FilteredData.root";
  
  TimeAndTotPmt2Plot(fileName, 31, 50, 25000, 70);
}


// get all the histos from file and store in histos array
void TimeAndTotPmt2Plot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq, UInt_t maxTime) {
  std::cout << std::setprecision(15) << std::endl;
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // histo and canvas
  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH2F *histo = new TH2F("histo", "histo", 300, 0, 300, (upperTot-lowerTot), lowerTot, upperTot);
  histo->SetTitle("Time between hits and ToT; Time Between Subsequent Hits/ns; ToT");

  // variabes used in for loop
  double signalTime = 0; double nextSignalTime = 0; double diff = 0;
  UInt_t intTot;
  int maxIndex = signalTree->GetEntries();
  
  // loop while there is still a signal to check
  for (int i = 0; i < maxIndex-1; i++) {

    // get first first signal
    signalTree->GetEntry(i);

    if ( (pomId != 162678077 && channel != 2) && (pomId != 16268300 && channel != 24) ) {
      signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
      intTot = (int) tot;

      // get second signals
      signalTree->GetEntry(i+1);

      if  ( (pomId != 162678077 && channel != 2) && (pomId != 16268300 && channel != 24) ) {
	nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);      
    
	// get difference in times and add to histo
	diff = nextSignalTime - signalTime; 
	histo->Fill(diff, intTot);
      }
    }
  }

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.08);
  gStyle->SetLabelSize(0.05, "XY");
  gStyle->SetTitleSize(0.05,"XY");
  gStyle->SetTitleOffset(1,"X");
  gStyle->SetTitleOffset(.7,"Y");
  
  //histo->GetZaxis()->SetRangeUser(0, maxFreq);
  histo->GetXaxis()->SetRangeUser(0, maxTime);
  histo->Draw("COLZ");
  histo->UseCurrentStyle();
}
