/*
for a file plots the time between hits and tot on 2d histo but only if the two signals are not from the same pmt
*/

#include "PosFuns.cpp"


void TimeAndTotPmtPlot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq, UInt_t maxTime);


void TimeAndTotPmt() {
  //TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "../../datafiles/FilteredData.root";
  TimeAndTotPmtPlot(fileName, 31, 50, 200, 60);
}


// get all the histos from file and store in histos array
void TimeAndTotPmtPlot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq, UInt_t maxTime) {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH2F *histo = new TH2F("histo", "histo", maxTime, 0, maxTime, upperTot - lowerTot, lowerTot, upperTot);
  histo->SetTitle("Time between hits and ToT; Time Between Subsequent Hits/ns; ToT/ns");

  //TH1F *samePmtHisto = new TH1F("samePmtHisto", "histo", 30*19, 0, 30*19);
  TH1F *samePmtHisto = new TH1F("samePmtHisto", "histo", 300, 0, 300);
  samePmtHisto->SetTitle("Signal pairs from same pmt; PMT Position; Frequency");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // get all the planes that have convig data
  std::vector<UInt_t> bcPlanes;
  GetPlanes(& bcPlanes, 2, "../../");

  double signalTime = 0;
  double nextSignalTime = 0;
  double diff = 0;
  bool isBeside;
  int maxIndex = signalTree->GetEntries();
  int signalIndex; // var to change in loop so i remains the loop loc
  
  // loop while there is still a signal to check
  for (int i = 0; i < maxIndex; i++) {
    std::cout << i << std::endl;
    
    signalIndex = i;    
    signalTree->GetEntry(signalIndex); // locate data in tree

    Signal signal1(pomId, channel, (int) tot, CombineTimeStamps(timeStamp_s, timeStamp_ns, offset));
    Signal signal2 = signal1; // initialise signal2 as blank signal


    // loop until find signal not incident on the same pmt as first one
    while ( (signal1.pomId == signal2.pomId && signal1.channel == signal2.channel) && (signalIndex < maxIndex) ) {    

      signalIndex++; // go to next signal

      // set signal2 
      signalTree->GetEntry(signalIndex);
      signal2.pomId = pomId;
      signal2.channel = channel;
      signal2.tot = (int) tot;
      signal2.signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

      if ( signal1.pomId == signal2.pomId && signal1.channel == signal2.channel ) { // if same pmt
      
	AddToPmtPosFreqPlot(samePmtHisto, signal1, bcPlanes); // add to histo
      }
    }

    diff = signal2.signalTime - signal1.signalTime; // difference between two times
    histo->Fill(diff, signal1.tot);
  }

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.08);
  gStyle->SetLabelSize(0.04, "XY");
  gStyle->SetTitleSize(0.05,"XY");
  gStyle->SetTitleOffset(0.8,"X");
  gStyle->SetTitleOffset(1,"Y");

  //histo->GetZaxis()->SetRangeUser(0, maxFreq);
  histo->Draw("COLZ");
  histo->UseCurrentStyle();

  TCanvas *c2 = new TCanvas("c2", "c2", 1000, 600);

  c2->cd();
  samePmtHisto->Draw();
  samePmtHisto->UseCurrentStyle();
  
}
