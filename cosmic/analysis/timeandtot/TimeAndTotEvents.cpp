/*
for a an array of events plots 2d hist of time between two hits and tot of second hit
*/

//#include "TimeAndTotPosFilter.cpp"
#include "AdjustmentFuns.cpp"


void TimeAndTotEventsPlot(std::vector<Event> events, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq, Double_t width);


void TimeAndTotEvents() {
  
  //TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "../../datafiles/FilteredForTot.root";
  std::vector<Event> events;  // vector to hold all the events

  Double_t width = 20;
  GetEvents(fileName, events, width); // number is the width (from ClusteringVariable.cpp)
  TimeAndTotEventsPlot(events, 31, 50, 200, width);
}


// get all the histos from file and store in histos array
void TimeAndTotEventsPlot(std::vector<Event> events, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq, Double_t width) {
 
  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH2F *histo = new TH2F("histo", "histo", width, 0, width, upperTot-lowerTot, lowerTot, upperTot);
  histo->SetTitle("Time between hits and ToT; Time Between Subsequent Hits/ns; ToT");

  
  Double_t time1; Double_t time2; Double_t timeDiff;
  Double_t pomId1; Double_t pomId2;
  Double_t timeAdjust;

  // get cable lengths
  std::vector<double> cableLengths;
  GetCableLengths(& cableLengths);

  // iterator and loop through all the events
  vector<Event>::iterator event_it;
  for (event_it = events.begin(); event_it != events.end(); event_it++) {

    //iterator and loop for signals in an event
    std::list<Signal>::iterator signal_it = event_it->signals.begin(); 
    //for (signal_it = signal_it->signals.begin(); signal_it != event_it->signals.end(); signal_it++) {
    while (signal_it != event_it->signals.end()) {

      timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(signal_it->pomId, signal_it->channel), cableLengths);
      time1 = signal_it->signalTime - timeAdjust;
      pomId1 = signal_it->pomId; 
      
      signal_it++;

      timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(signal_it->pomId, signal_it->channel), cableLengths);
      time2 = signal_it->signalTime - timeAdjust;
      pomId2 = signal_it->pomId;

      //if (pomId1 == pomId2) { // add/remove comments for pomId check

	timeDiff = time2 - time1;	
   	histo->Fill(timeDiff, signal_it->tot);
	
	// }
    }
 
    histo->SetStats(1);
    histo->GetZaxis()->SetRangeUser(0, maxFreq);
    histo->Draw("COLZ");
  }
}
