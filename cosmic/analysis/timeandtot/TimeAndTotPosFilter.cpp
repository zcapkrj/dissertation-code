/*
for a file plots 2d hist of time between two hits and tot of second hit, will only plot if the two hits are beside each other on the bottom cap
*/

#include "AdjustmentFuns.cpp"


void TimeAndTotPosFilterPlot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq);
void GetPMTx(float & x, int pmtPos, int _phx, int _phy);
void GetPMTy (float & y, int _pomType, int pmtPos, int _phx, int _phy);
void getXYcoords(std::string line, int channel, float & x, float & y);
bool IsBeside(Signal signal1, Signal signal2);


void TimeAndTotPosFilter() {
  //TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "../../datafiles/FilteredData.root";
  TimeAndTotPosFilterPlot(fileName, 31, 50, 200);
}


// get all the histos from file and store in histos array
void TimeAndTotPosFilterPlot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq) {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH2F *histo = new TH2F("histo", "histo", 300, 0, 300, upperTot - lowerTot, lowerTot, upperTot);
  histo->SetTitle("Time between hits and ToT; Time Between Subsequent Hits/ns; ToT");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // get cable lengths
  std::vector<double> cableLengths;
  GetCableLengths(& cableLengths);
  
  // vars for loop
  double signalTime = 0;
  double nextSignalTime = 0;
  double diff = 0;
  bool isBeside;
  int maxIndex = signalTree->GetEntries(); std::cout << maxIndex << std::endl;
  int signalIndex; // var to change in loop so i remains the loop loc
  Double_t timeAdjust;
  
  // loop while there is still a signal to check
  for (int i = 0; i < maxIndex; i++) {

    std::cout << i << std::endl;
    
    isBeside = false; // init as false
    signalIndex = i;
        
    signalTree->GetEntry(signalIndex); // locate data in tree

    timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(pomId, channel), cableLengths);
    Signal signal1(pomId, channel, (int) tot, CombineTimeStamps(timeStamp_s, timeStamp_ns, offset) - timeAdjust);

    if (signal1.tot > 30) { // check if first signal greater than 30

      Signal signal2; // initialise signal2 as blank signal

      while ( ((signal2.tot < 31) || (isBeside == false)) && (signalIndex < maxIndex) ) { // keep going to next signal until finds one beside original
	
	signalIndex++; // go to next signal

	// set signal2 
	signalTree->GetEntry(signalIndex);
	signal2.pomId = pomId;
	signal2.channel = channel;
	signal2.tot = (int) tot;
	signal2.signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

	isBeside = IsBeside(signal1, signal2); // if beside signal1
	//std::cout << isBeside << std::endl;
      }

      timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(pomId, channel), cableLengths);
      diff = (signal2.signalTime - timeAdjust) - signal1.signalTime; // difference between two times
      histo->Fill(diff, signal1.tot);
    }
  }

  histo->SetStats(1);
  histo->GetZaxis()->SetRangeUser(0, maxFreq);
  histo->Draw("COLZ");
}


// checks if two signals are beside each other
bool IsBeside(Signal signal1, Signal signal2) {
  
  TString id1 = ToString(signal1.pomId); // convert pomId to string
  TString id2 = ToString(signal2.pomId); // convert pomId to string
  float x1; float x2; float y1; float y2;
  
  ifstream myfile ("../../datafiles/pomPmtPositionMap.txt"); // open config file
  std::string line; // varible to hold each line
    
  if (myfile.is_open()) { // if file was opened

    while ( getline(myfile, line) ) { // read every line of file 

      if ( line.find(id1) != std::string::npos) { // check if right pomid

	getXYcoords(line, signal1.channel, x1, y1);
      }

      if ( line.find(id2) != std::string::npos) { // check if right pomid

	getXYcoords(line, signal2.channel, x2, y2);
      }
    }
    myfile.close();

    if ( std::abs(x1-x2) == 1 || std::abs(y1-y2) ) {
      return true;
    }
    else {
      return false;
    }
  }
  
  return false;
}


void getXYcoords(std::string line, int channel, float & x, float & y) {
  
  std::istringstream s(line); // stream for line
  std::vector<string> elements; // variable to hold each element of line
  string element; // hold each element of line 
  
  while (s >> element) {
    elements.push_back(element); // add element to end of elements vector
  }

  int type_index           = std::stoi(elements[1]);
  int pom_x_position_index = std::stoi(elements[2]);
  int pom_y_position_index = std::stoi(elements[3]);
  int pmtPos               = std::stoi(elements[5 + channel]);
  
  GetPMTx(x, pmtPos, pom_x_position_index, pom_y_position_index);
  GetPMTy(y, type_index, pmtPos, pom_x_position_index, pom_y_position_index);
}


void  GetPMTx (float & x, int pmtPos, int _phx, int _phy) {

  x = -999;

  if (pmtPos >= 0) {
    x = -((_phx-1) * 5 +  ( (pmtPos - 1) % 5) + 1);
  }
}
 

void GetPMTy (float & y, int _pomType, int pmtPos, int _phx, int _phy) {
  
  y = -999;
  
  if(_pomType == 0 && pmtPos > 0) {
    y = ( 5*(8 - _phy-0.6 * abs(_phx-1)) -  ((pmtPos-1) / 5) );
  }
  else if(_pomType == 2 ) {
    y = (   6*( 7.8 - _phy -0.6 * abs(_phx-1) ) -  ((pmtPos-1)/5));  // 7.8 not correct 
  }
}
