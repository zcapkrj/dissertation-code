/*
for a file plots 2d hist of time between two hits (over 30 tot) and tot of second hit
*/

#include "AdjustmentFuns.cpp"

void RawTimeAndTotPlot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq);


void RawTimeAndTot() {
  //TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "../../datafiles/FilteredData.root";
  RawTimeAndTotPlot(fileName, 31, 100, 1000);
}


// get all the histos from file and store in histos array
void TotCheckedPlot(TString fileName, UInt_t lowerTot, UInt_t upperTot, UInt_t maxFreq) {
  std::cout << std::setprecision(15) << std::endl;
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // histo and canvas
  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH2F *histo = new TH2F("histo", "histo", 300, 0, 300, upperTot-lowerTot, lowerTot, upperTot);
  histo->SetTitle("Time between hits and ToT; Time Between Subsequent Hits/ns; ToT");

  // variabes used in for loop
  double signalTime = 0; double nextSignalTime = 0; double diff = 0;
  Double_t int_tot1; Double_t int_tot2;
  int maxIndex = signalTree->GetEntries();
  
  // loop while there is still a signal to check
  for (int i = 0; i < maxIndex;) {
   
    signalTree->GetEntry(i);
    signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
    int_tot1  = (int) tot; // get first tot
    pomId1 = pomId;
    //std::cout << pomId1 << std::endl;

    if (int_tot1 > 30) { // check if first signal greater than 30
      int_tot2 = 0;
      pomId2 = 0;


      // remove comments for pomId check as well
      while ( ((int_tot2 < 30) /* || (pomId1 != pomId2) */) && (i < maxIndex) ) {
	
	i++;
	signalTree->GetEntry(i);
	nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
	int_tot2  = (int) tot;
	pomId2 = pomId;
      }

      timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(pomId, channel), cableLengths); // time adjustment for 2nd hit
      diff = (nextSignalTime - timeAdjust) - signalTime; // difference between two times
      histo->Fill(diff, int_tot1);
    }
    else {
      i++;
    }
  }

  histo->SetStats(111); // need to move stats box
  histo->GetZaxis()->SetRangeUser(0, 500);
  histo->Draw("COLZ");
}
