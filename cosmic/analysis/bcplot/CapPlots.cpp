/*
takes an event and plots on a diagram of the bottom cap
*/

#include "../../clustering/ClusterVariable.cpp"


void PlotEvent(Event event, TH2F * hhit_bc_map, TCanvas *c);
float GetPMTx(int pmtPos, int _phx, int _phy);
float GetPMTy (int _pomType, int pmtPos, int _phx, int _phy);
void DrawBCLines(TCanvas *c);
void DrawTCLines( TCanvas *cc);



void CapPlots() {

  TString fileName = "../../datafiles/FilteredBC.root";
  //TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  std::vector<Event> events;  // list to hold all the events

  GetEvents(fileName, events, 5, 10); // for ClusterVariable.cpp

  // set up axis and histo
  TCanvas *c = new TCanvas("c", "c", 600, 600); // create canvas
  c->SetRightMargin(0.15); // adjust for label being cut off
  TH2F *hhit_bc_map = new TH2F("hhit_bc_map", ";X;Y", 12*5, -6*5, 6*5, 6*4+2*10, 0, 6*4+2*10);
  hhit_bc_map->SetTitle("Bottom Cap"); 
  hhit_bc_map->GetZaxis()->SetTitle("ToT");

  PlotEvent(events[5], hhit_bc_map, c);
  DrawBCLines(c);
}


void PlotEvent(Event event, TH2F *hhit_map, TCanvas *c) {

  c->cd(); // move to this canvas
  
  gStyle->SetOptStat(0);
  gStyle->SetPalette(kDeepSea);
  //  gStyle->SetPalette(kRust);
  //  gStyle->SetPalette(kSolar);

  std::vector<Signal>::iterator it;
  for (it = event.signals.begin(); it != event.signals.end(); it++) { // loop through all the signals in event

    TString id = ToString(it->pomId); // convert pomId to string
    ifstream myfile ("../../datafiles/pomPmtPositionMap.txt"); // open config file
    std::string line; // varible to hold each line

    // initialise signal variables
    int type_index; 
    int pom_x_position_index;
    int pom_y_position_index;
    int pmtPos;
    
    if (myfile.is_open()) { // if file was opened

      while ( getline(myfile, line) ) { // read every line of file 

	if ( line.find(id) != std::string::npos) { // check if right pomid

	  std::istringstream s(line); // stream for line
	  std::vector<string> elements; // variable to hold each element of line
	  string element; // hold each element of line 

	  while (s >> element) {
	    elements.push_back(element); // add element to end of elements vector
	  }

	  type_index           = std::stoi(elements[1]);
	  pom_x_position_index = std::stoi(elements[2]);
	  pom_y_position_index = std::stoi(elements[3]);
	  pmtPos               = std::stoi(elements[5 + it->channel]);

	  std::cout << id <<  " " << pmtPos << " " << it->signalTime << std::endl;

	  float x = GetPMTx(pmtPos, pom_x_position_index, pom_y_position_index);
	  float y = GetPMTy(type_index, pmtPos, pom_x_position_index, pom_y_position_index);
	  
	  hhit_map->Fill(x, y, it->tot); // fill histo of signal

	}
      }

      myfile.close();
    }
  }

  hhit_map->Draw("COLZ");
}


float GetPMTx (int pmtPos, int _phx, int _phy) {

  float x= -999;

  if (pmtPos < 0) {

    pmtPos = 5*(pmtPos*-1 -1) + 2;
    x = -((_phx-1) * 5 +  ( (pmtPos - 1) % 5) + 1);
  }
  else {
    x = -((_phx-1) * 5 +  ( (pmtPos - 1) % 5) + 1);
  }
  
  return x; 
}
 

float GetPMTy (int _pomType, int pmtPos, int _phx, int _phy) {
  
  float y = -999;
  
  if(_pomType == 0 && pmtPos > 0) {
    y = ( 5*(8 - _phy-0.6 * abs(_phx-1)) -  ((pmtPos-1) / 5) );
  }
  else if(_pomType == 2 ) {
    y = (   6*( 7.8 - _phy -0.6 * abs(_phx-1) ) -  ((pmtPos-1)/5));  /// 7.8 not correct - need to redefine histos
  }
  else if (pmtPos < 0) {
    pmtPos = 5*(pmtPos*-1 -1) + 2;
    y = ( 5*(8 - _phy-0.6 * abs(_phx-1)) -  ((pmtPos-1) / 5) );
  }
  
  
  return y;
}


void DrawBCLines(TCanvas *cc) {

  cc->cd();

  TLine *lbx[600];
  TLine *lby[40];
  float xx,yy;
  float x1,x2,y1,y2;
  float check1=0;
  float check2=0;
  int ny[] = {1,2,3,3,4,4, 4,4,3,3,2,1};
  
  for( int ix=0; ix<12; ++ix){
    xx = -6*5 +ix*5;
    y1 = (ix < 6) ?  6*  ( 7.8 - ny[ix]-1 - 0.6 *abs(ix-5)) +1: 6*  ( 7.8 - ny[ix]-1 - 0.6 *abs(ix-6)) +1;
    y2 =  y1 + 6*(ny[ix]);
    if(y1>0){
      lby[2*ix] = new TLine( xx, int(y1), xx, int(y2) );
      lby[2*ix]->SetLineWidth(2);
      lby[2*ix]->Draw("same");
      
      lby[2*ix+1] = new TLine( xx+5, int(y1), xx+5, int(y2) );
      lby[2*ix+1]->SetLineWidth(2);
      lby[2*ix+1]->Draw("same");
    }
    for( int iy=1; iy<6; ++iy){
      if( iy-1 >  ny[ix] ) continue; 
      x1 = xx;
      x2 = xx+5;
      yy = (ix < 6) ?  6*  ( 7.8 - iy - 0.6 *abs(ix-5)) +1 : 6*  ( 7.8 - iy - 0.6 *abs(ix-6)) +1;
      lbx[20*ix+iy] = new TLine( x1, int(yy), x2, int(yy) );
      lbx[20*ix+iy]->SetLineWidth(2);
      lbx[20*ix+iy]->Draw("same");	
    }// for iy
  } // for ix

} // DrawBCLines



void DrawTCLines( TCanvas *cc) {
  cc->cd();

  TLine *ltx[600];
  TLine *lty[40];
  float xx,yy;
  float x1,x2,y1,y2;
  float check1=0;
  float check2=0;
  int ny[] = {1,2,3,4,4,5, 5,4,4,3,2,1};
  
  // for( int ix=0; ix<12; ++ix){
  for( int ix=0; ix<12; ++ix){
    xx = -6*5 +ix*5;
    //  y1 = (ix < 6) ?  5*  ( 8 - ny[ix]-1 - 0.3 *abs(ix-5)) +1: 5*  ( 8 - ny[ix]-1 - 0.3 *abs(ix-6)) +1;
    y1 = (ix < 6) ?  5*  ( 8 - ny[ix]-1 - 0.6 *abs(ix-5)) +1: 5*  ( 8 - ny[ix]-1 - 0.6 *abs(ix-6)) +1;
    y2 =  y1 + 5*(ny[ix]);
    //    cout << ix << " " << ny[ix] << " "<< y1 << " " << y2 << endl;
    if(y1>0){
      lty[2*ix] = new TLine( xx, int(y1), xx, int(y2) );
      lty[2*ix]->SetLineWidth(2);
      lty[2*ix]->Draw("same");
      
      lty[2*ix+1] = new TLine( xx+5, int(y1), xx+5, int(y2) );
      lty[2*ix+1]->SetLineWidth(2);
      lty[2*ix+1]->Draw("same");
    }
    for( int iy=1; iy<7; ++iy){
      if( iy-1 >  ny[ix] ) continue; 
      x1 = xx;
      x2 = xx+5;
      // yy = (ix < 6) ?  5*  ( 8 - iy - 0.3 *abs(ix-5)) +1 : 5*  ( 8 - iy - 0.3 *abs(ix-6)) +1;
      yy = (ix < 6) ?  5*  ( 8 - iy - 0.6 *abs(ix-5)) +1 : 5*  ( 8 - iy - 0.6 *abs(ix-6)) +1;
      ltx[20*ix+iy] = new TLine( x1, int(yy), x2, int(yy) );
      ltx[20*ix+iy]->SetLineWidth(2);
      ltx[20*ix+iy]->Draw("same");	
    }// for iy
  } // for ix

} // DrawTCLines

