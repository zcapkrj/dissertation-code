// MAKE FLOW CHART FOR THIS IN DISS

// turn this into class with the different methods and plotting types

#include <list>
#include <string>
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TList.h>
#include <TF1.h>
#include <TPaveStats.h>

// global variables
Double_t s_To_ns = 1000000000;
Double_t maxTime = 10 * s_To_ns;
Double_t width = 20;


// represetns a single signal on a pmt
struct Signal {
  UInt_t pomId;
  UInt_t channel;
  Char_t tot;

  Signal(UInt_t getPomId, UInt_t getChannel, Char_t getTot) {
    pomId   = getPomId;
    channel = getChannel;
    tot     = getTot;
  }
};

// represents a collection of signals at a specific time
struct Event {
  Double_t timeOfEvent;
  std::list<Signal> signals;
};


// init functions
TString ToString(int num);
void GetSignals(vector<Event>& events, TString fileName);
void plotCluster(vector<Event>& events);
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset);


void ClusterVariable() {
  //TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "FilteredToT.root";
  
  std::vector<Event> events;  // list to hold all the events
  GetSignals(events, fileName); // get and store all the events in events list
}


// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// function takes the times from the root file and combines them for the ns timestamp
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset) {
  
  return (timeStamp_s - offset) * s_To_ns + timeStamp_ns;
}


void GetSignals(std::vector<Event>& events, TString fileName) {
  std::cout << std::setprecision(15) << std::endl;

  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;
  
  Double_t signalTime = 0; // time of current signal
  Double_t nextSignalTime = 0; // time of signal being check in range
  UInt_t signalIndex = 0; // place holder for within the for loop

  UInt_t lastSignalIndex = signalTree->GetEntries();
  //UInt_t lastSignalIndex = 100000;
  
  for (UInt_t i = 0; i < lastSignalIndex; i++) {

    // initialise starting conditions
    signalIndex = i; // set the initial hit index
    Event event; // init event struct

    signalTree->GetEntry(signalIndex); // get first hit entry 
    signalTime = nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset); // calc time of hit
    
    // check if next signal within window, if so add to event object and get next signal
    while ((nextSignalTime - signalTime) < width) {

      signalTime = nextSignalTime; // update signal time
      
      Signal signal(pomId, channel, tot); // creat signal for hit
      event.signals.push_back(signal); // add hit to event

      // get next hit and time of hit
      signalIndex++;
      signalTree->GetEntry(signalIndex);
      nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
    }

    if (event.signals.size() > 10) { // if size of event large enough add to events array
      event.timeOfEvent = signalTime;

      //std::cout << signalTime/s_To_ns << std::endl;
      
      events.push_back(event);
      i = signalIndex; // start at last hit that didnt make the cut
    }
    else {
      i++; // start at 1 plus orginal pos
    }
  }
}





void plotCluster(std::vector<Event>& events) {
  std::cout << std::setprecision(15) << std::endl;
  
    // define canvas for the ratio of ToT peaks
  TCanvas *c = new TCanvas("c", "c", 1000, 600);

  // create histograms for ratios and ADC of peaks
  TH1F *histo = new TH1F("histo", "Event Signals", maxTime / 1000, 0, maxTime);

  // setting the title and axis labels
  histo->SetTitle("Event Signals Variable Bins; Time/s; Signals in Event");

  Double_t time;
  UInt_t eventSize;
    
  std::vector<Event>::iterator it;
  for (it = events.begin(); it != events.end(); it++)
    {
      // Access the object through iterator
      time = (UInt_t) ((it->timeOfEvent) / 1000);
      eventSize = it->signals.size();
	
      histo->SetBinContent(time, eventSize);
      
      //std::cout << time << std::endl;
}
  
  histo->SetStats(111);
  histo->GetXaxis()->SetRangeUser(9 * s_To_ns, 9.011 * s_To_ns);
  histo->Draw();
  histo->GetXaxis()->SetLimits(0, 10);
}
