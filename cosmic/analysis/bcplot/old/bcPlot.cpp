#include <TROOT.h>


// define the same of the file with all the events in it 

void bcPlot() {
  gStyle->SetOptStat(0);
  gStyle->SetPalette(kDeepSea);
  //  gStyle->SetPalette(kRust);
  //  gStyle->SetPalette(kSolar);
  
   TLegend *leg = new TLegend(0.6, 0.7, 0.9, 0.9);
   leg->SetBorderSize(0);
   leg->SetFillStyle(0);


   //  gStyle->SetOptStat("e");
   TCanvas *c1 = new TCanvas("c1");
     
   
     h_tot->Draw("hist");
     //gPad->Update();
     //TPaveStats *st1 = (TPaveStats*)h_tot->FindObject("stats");
     //st1->SetTextColor(kBlue);
     //     st1->SetY1NDC();
     // st1->SetY2NDC();

     hslice_tot->SetStats(1);
     hslice_tot->SetLineColor(kRed+1);
     hslice_tot->Draw("histsame");

     h_tot->SetTitle(";ToT [ns];Fraction of Hits");

     leg->AddEntry(h_tot, "All Hits", "L");
     leg->AddEntry(hslice_tot, "Coincidence Hits", "L");
     leg->Draw();

     DrawBCLines(c1);
}


void DrawBCLines( TCanvas *cc){

  cc->cd();

  TLine *lbx[600];
  TLine *lby[40];
  float xx,yy;
  float x1,x2,y1,y2;
  float check1=0;
  float check2=0;
  int ny[] = {1,2,3,3,4,4, 4,4,3,3,2,1};
  for( int ix=0; ix<12; ++ix){
    xx = -6*5 +ix*5;
    y1 = (ix < 6) ?  6*  ( 7.8 - ny[ix]-1 - 0.6 *abs(ix-5)) +1: 6*  ( 7.8 - ny[ix]-1 - 0.6 *abs(ix-6)) +1;
    y2 =  y1 + 6*(ny[ix]);
    if(y1>0){
      lby[2*ix] = new TLine( xx, int(y1), xx, int(y2) );
      lby[2*ix]->SetLineWidth(2);
      lby[2*ix]->Draw("same");
      
      lby[2*ix+1] = new TLine( xx+5, int(y1), xx+5, int(y2) );
      lby[2*ix+1]->SetLineWidth(2);
      lby[2*ix+1]->Draw("same");
    }
    for( int iy=1; iy<6; ++iy){
      if( iy-1 >  ny[ix] ) continue; 
      x1 = xx;
      x2 = xx+5;
      yy = (ix < 6) ?  6*  ( 7.8 - iy - 0.6 *abs(ix-5)) +1 : 6*  ( 7.8 - iy - 0.6 *abs(ix-6)) +1;
      lbx[20*ix+iy] = new TLine( x1, int(yy), x2, int(yy) );
      lbx[20*ix+iy]->SetLineWidth(2);
      lbx[20*ix+iy]->Draw("same");	
    }// for iy
  } // for ix

} // DrawBCLines




