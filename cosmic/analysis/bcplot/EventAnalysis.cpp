/* 
from the events pick out the ones with the highest average tot and with the most signals
 */

#include "CapPlots.cpp"


void GetLargestTotEvent(std::vector<Event> events, Event &event1, Event &event2);
Double_t GetAvgTot(Event event);
void GetMostSignalsEvent(std::vector<Event> events, Event &event1, Event &event2);


void EventAnalysis() {

  TString fileName = "../../datafiles/FilteredBC.root";
  //TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";

  // get all the events
  std::vector<Event> events; 
  GetEvents(fileName, events, 5, 10); 

  Event event1; Event event2;
  //GetLargestTotEvent(events, event1, event2);
  GetMostSignalsEvent(events, event1, event2);

  // set up axis and histo
  TCanvas *c = new TCanvas("c", "c", 600, 600); // create canvas
  c->SetRightMargin(0.15); // adjust for label being cut off
  TH2F *hhit_bc_map = new TH2F("hhit_bc_map", ";X;Y", 12*5, -6*5, 6*5, 6*4+2*10, 0, 6*4+2*10);
  hhit_bc_map->SetTitle("Bottom Cap"); 
  hhit_bc_map->GetZaxis()->SetTitle("ToT");
  
  PlotEvent(event1, hhit_bc_map, c);
  DrawBCLines(c); // draw the lines
}


// gets the two events with the highest average tot
void GetLargestTotEvent(std::vector<Event> events, Event &event1, Event &event2) { 

  Event holderEvent; 
  Double_t currentLargestTot = 0; Double_t secondLargestTot = 0;
  Double_t newAvgTot = 0;
  
  for (int i = 0; i < events.size(); i++) {
    
    holderEvent = events[i];
    newAvgTot = GetAvgTot(holderEvent);

    if ( newAvgTot > currentLargestTot ) { // check if the largest 
      
      currentLargestTot = newAvgTot;
      event1 = holderEvent;
    }
    else if ( newAvgTot > secondLargestTot) { // if not check if the second largest
      
      secondLargestTot = newAvgTot;
      event2 = holderEvent;
    }
  }
}


// gets the average tot of an event
Double_t GetAvgTot(Event event) {

  UInt_t totalTot = 0;
  
  std::vector<Signal>::iterator it;
  for (it = event.signals.begin(); it != event.signals.end(); it++) { // loop over every signal of event

    totalTot += (int) it->tot;
  }

  return totalTot/event.signals.size();
}


// get 2 events with the most signals
void GetMostSignalsEvent(std::vector<Event> events, Event &event1, Event &event2) { 

  Event holderEvent; 
  Double_t currentMostSignals = 0; Double_t secondMostSignals = 0;
  Double_t newMostSignals = 0;
  
  for (int i = 0; i < events.size(); i++) {

    holderEvent = events[i];
    newMostSignals = holderEvent.signals.size();
    
    if ( newMostSignals > currentMostSignals ) {
      
      currentMostSignals = newMostSignals;
      event1 = holderEvent;
    }
    else if (newMostSignals > secondMostSignals) {

      secondMostSignals = newMostSignals;
      event2 = holderEvent;
    }
  }
}

