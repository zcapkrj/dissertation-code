#include "../bcplot/EventAnalysis.cpp"


Event GetVetosWindow(TString fileName, Event event, Double_t minTime, Double_t maxTime);
void PlotTcEvent(Event event, TH2F *capMap, TCanvas *c);
Event GetVetoClostestToEvent(TString fileName, Event event);
Event GetVetoAftertToEvent(TString fileName, Event event);
Event GetVetoSecondClosestToEvent(TString fileName, Event event);

void PlotVetos() {

  TString fileNameEvents = "../../datafiles/FilteredBC.root";
  TString fileNameVetos = "../../datafiles/TimeAdjustedTopCapVetos.root";

  Double_t lightSpeedWater = 255000000; // speed of light in water 
  Double_t minTime = (int) 10 / lightSpeedWater * s_To_ns; Double_t maxTime = (int) 27 / lightSpeedWater * s_To_ns; // min and max time for a photon to rach the bottom cap using the dimensions of the dector
  
  // set up axis and histo for bc
  TCanvas *bc_c = new TCanvas("bc_c", "bc_c", 600, 600); // create canvas
  bc_c->SetRightMargin(0.15); // adjust for label being cut off
  TH2F *hhit_bc_map = new TH2F("hhit_bc_map", ";X;Y", 12*5, -6*5, 6*5, 6*4+2*10, 0, 6*4+2*10);
  hhit_bc_map->SetTitle("Bottom Cap Event"); 
  hhit_bc_map->GetZaxis()->SetTitle("ToT/ns");

  // set up axis and histo for top cap
  TCanvas *tc_c_before = new TCanvas("tc_c_before", "tc_c_before", 600, 600); // create canvas
  tc_c_before->SetRightMargin(0.15); // adjust for label being cut off
  TH2F *hhit_tc_map_before = new TH2F("hhit_tc_map_before", ";X;Y", 12*5, -6*5, 6*5, 5*5+2*9,  0, 5*5+2*9 );
  hhit_tc_map_before->SetTitle("Top Cap Clostest Veto Signal Before Event"); 
  hhit_tc_map_before->GetZaxis()->SetTitle("ToT/ns");

  // set up axis and histo for top cap
  TCanvas *tc_c_second_before = new TCanvas("tc_c_second_before", "tc_c_second_before", 600, 600); // create canvas
  tc_c_second_before->SetRightMargin(0.15); // adjust for label being cut off
  TH2F *hhit_tc_map_second_before = new TH2F("hhit_tc_map_second_before", ";X;Y", 12*5, -6*5, 6*5, 5*5+2*9,  0, 5*5+2*9 );
  hhit_tc_map_second_before->SetTitle("Top Cap Second Clostest Veto Signal Before Event"); 
  hhit_tc_map_second_before->GetZaxis()->SetTitle("ToT/ns");

  // set up axis and histo for top cap
  TCanvas *tc_c_after = new TCanvas("tc_after", "tc_after", 600, 600); // create canvas
  tc_c_after->SetRightMargin(0.15); // adjust for label being cut off
  TH2F *hhit_tc_map_after = new TH2F("hhit_tc_map_after", ";X;Y", 12*5, -6*5, 6*5, 5*5+2*9,  0, 5*5+2*9 );
  hhit_tc_map_after->SetTitle("Top Cap Clostest Veto Signal After Event"); 
  hhit_tc_map_after->GetZaxis()->SetTitle("ToT/ns");

  // get all the events from the data
  std::vector<Event> events; 
  GetEvents(fileNameEvents, events, 5, 10); 
 
  Event event1; Event event2;
  //GetLargestTotEvent(events, event1, event2);
  GetMostSignalsEvent(events, event1, event2);
  
  PlotEvent(event2, hhit_bc_map, bc_c);
  DrawBCLines(bc_c);

  std::cout << "" << std::endl;
  
  Event vetoEventBefore = GetVetoClostestToEvent(fileNameVetos, event2);
  PlotEvent(vetoEventBefore, hhit_tc_map_before, tc_c_before);
  DrawTCLines(tc_c_before);

  std::cout << "" << std::endl;

  Event vetoEventAfter = GetVetoAftertToEvent(fileNameVetos, event2);
  PlotEvent(vetoEventAfter, hhit_tc_map_after, tc_c_after);
  DrawTCLines(tc_c_after);

  std::cout << "" << std::endl;

  Event vetoEventSecondBefore = GetVetoSecondClosestToEvent(fileNameVetos, event2);
  PlotEvent(vetoEventSecondBefore, hhit_tc_map_second_before, tc_c_second_before);
  DrawTCLines(tc_c_second_before);


}

Event GetVetosWindow(TString fileName, Event event, Double_t minTime, Double_t maxTime) {

  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  Double_t eventTime = event.timeOfEvent;
  Double_t vetoTime = 0;
  UInt_t nEntries = signalTree->GetEntries();
  Event vetoSignals; // create an event of veto signals

  for (int i = 0; i < nEntries; i++) {

    signalTree->GetEntry(i); // locate data in tree
    vetoTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
    
    if ( (vetoTime > (eventTime-maxTime)) && (vetoTime < eventTime-minTime) ) { // look for signals in a window

      Signal signal(pomId, channel, tot, vetoTime); // create signal object for veto hit
      vetoSignals.signals.push_back(signal); // add veto signal to event
    }
  }

  return vetoSignals;
}


Event GetVetoClostestToEvent(TString fileName, Event event) {

  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  Double_t eventTime = event.timeOfEvent;
  Double_t vetoTime = 0;
  Double_t timeDif; Double_t smallestTimeDif = 1 * s_To_ns;
  UInt_t nEntries = signalTree->GetEntries();
  Event vetoSignal;
  vetoSignal.signals.push_back(Signal());

  
  for (int i = 0; i < nEntries; i++) {

    signalTree->GetEntry(i);
    vetoTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

    timeDif = (eventTime - vetoTime);
    
    if ( timeDif < smallestTimeDif && vetoTime < eventTime) { // checking for clostest veto to signal

      smallestTimeDif = timeDif;
      Signal signal(pomId, channel, tot, vetoTime); // create signal object for veto hit
      vetoSignal.signals = {signal}; // using event, as plot function event already made 
    }
  }

  std::cout << "Time of first signal in event: " << eventTime << std::endl; 
  std::cout << "Veto Signal Time: " << vetoSignal.signals[0].signalTime << std::endl; 
  std::cout << "Time difference between event and clostest veto trigger before event: " << smallestTimeDif << std::endl;
  
  return vetoSignal;
}


Event GetVetoAftertToEvent(TString fileName, Event event) {

  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  Double_t eventTime = event.timeOfEvent;
  Double_t vetoTime = 0;
  Double_t timeDif; Double_t smallestTimeDif = 1 * s_To_ns;
  UInt_t nEntries = signalTree->GetEntries();
  Event vetoSignal;
  vetoSignal.signals.push_back(Signal());

  
  for (int i = 0; i < nEntries; i++) {

    signalTree->GetEntry(i);
    vetoTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

    timeDif = std::abs(eventTime - vetoTime);

    
    if ( timeDif < smallestTimeDif && vetoTime > eventTime) {

      smallestTimeDif = timeDif;
      Signal signal(pomId, channel, tot, vetoTime); // create signal object for veto hit
      vetoSignal.signals = {signal}; // using event, as plot function event already made 
    }
  }

  std::cout << "Time of last signal in event: " << eventTime << std::endl; 
  std::cout << "Veto Signal Time: " << vetoSignal.signals[0].signalTime << std::endl;
  std::cout << "Time difference between event and clostest veto trigger after event: " << smallestTimeDif << std::endl;
  
  return vetoSignal;

  
}



Event GetVetoSecondClosestToEvent(TString fileName, Event event) {

  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  Double_t eventTime = event.timeOfEvent;
  Double_t vetoTime = 0;
  Double_t timeDif; Double_t smallestTimeDif = 1 * s_To_ns; Double_t secondSmallest = 1 * s_To_ns;
  UInt_t smallestIndex=0; UInt_t secondIndex;
  UInt_t nEntries = signalTree->GetEntries();
  Event vetoSignal;
  vetoSignal.signals.push_back(Signal());

  
  for (int i = 0; i < nEntries; i++) {

    signalTree->GetEntry(i);
    vetoTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

    timeDif = eventTime - vetoTime;

    //if (vetoTime < eventTime) {std::cout << timeDif << std::endl;}
    
    if ( timeDif < smallestTimeDif && vetoTime < eventTime ) { // checking for clostest veto to signal

      secondIndex = smallestIndex;
      secondSmallest = smallestTimeDif;

      smallestIndex = i;
      smallestTimeDif = timeDif; 
    }
  }

  signalTree->GetEntry(secondIndex);
  Signal signal(pomId, channel, tot, vetoTime); // create signal object for veto hit
  vetoSignal.signals = {signal}; // using event, as plot function event already made

  std::cout << "Time of first signal in event: " << eventTime << std::endl; 
  std::cout << "Veto Signal Time: " << vetoSignal.signals[0].signalTime << std::endl; 
  std::cout << "Time difference between event and second clostest veto trigger before event: " << secondSmallest << std::endl;
  
  return vetoSignal;
}

