/* 
make vector with all the planes in the file
*/

#include "PlotVetos.cpp"

std::vector<Double_t> GetPlanesInData (TString fileName);

void TcPlanesInData() {

  std::cout << std::setprecision(15) << std::endl; // increase print precision for debugging
  TString fileName= "../../datafiles/TopCapData.root";

  std::vector<Double_t> planes = GetPlanesInData(fileName);

  for (int i=0; i < planes.size(); i++) {std::cout << planes[i] << std::endl;}

  // get all the planes that have convig data
  //std::vector<UInt_t> tcPlanes;
  //GetPlanes(& tcPlanes, 0, "../../");
  //for (int i=0; i < tcPlanes.size(); i++) {std::cout << tcPlanes[i] << std::endl;}
}
