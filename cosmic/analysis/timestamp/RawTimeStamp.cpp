/*
for a file, plot all the time stamps
 */

#include "../../CoreFuns.cpp"

void PlotTimeStamps(TString fileName, TString title, Double_t maxTime, Double_t focus, TCanvas *c);


void RawTimeStamp() {

  TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  //TString fileName = "../../datafiles/FilteredForTot.root";

  TCanvas *c = new TCanvas("c", "c", 1500, 300);
  PlotTimeStamps(fileName, "TimeStamp frequency for 10 ms of raw data", 2,1, c); 
  //c->SetBottomMargin(0.3);
  //c->Divide(1,2,0.01,0.01,0); // divide canvas ( set for 30 channels)
  
  //c->cd(1);
  //PlotTimeStamps(fileName, "TimeStamp frequency of filtered data (>30ns ToT) ", 10, 999, c); // get and store all the events in events list (999 for no focus)

  //c->cd(2);
  //PlotTimeStamps(fileName, "Raw TimeStamp frequency zoomed into first 1 ms of second 1", 2, 1, c); 

}


void PlotTimeStamps(TString fileName, TString title, Double_t maxTime, Double_t focus, TCanvas *c) {
  std::cout << std::setprecision(15) << std::endl;
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  Double_t maxTime_ns = maxTime * s_To_ns; // convert to ns
  
  TH1F *histo = new TH1F("histo", "Raw Timestamps", maxTime_ns/10000, 0, maxTime_ns);
  histo->SetTitle(title +"; Time/s; Frequency");

  UInt_t signalIndex = 0;
  Double_t signalTime = 0;
  Double_t int_time;

  while (signalTime < maxTime_ns) {

    signalTree->GetEntry(signalIndex); 
    signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

    histo->Fill(signalTime);

    signalIndex++;
  }

  if (focus != 999) { // an out so can look at whole thing
    histo->GetXaxis()->SetRangeUser(focus * s_To_ns, (focus+0.011) * s_To_ns);
  }

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.08);
  gStyle->SetLabelSize(0.07, "XY");
  gStyle->SetTitleSize(0.08,"XY");
  gStyle->SetTitleOffset(.5,"X");
  gStyle->SetTitleOffset(.3,"Y");  
   
  histo->Draw();
  histo->UseCurrentStyle();
  histo->GetXaxis()->SetLimits(0, maxTime);


  
}

