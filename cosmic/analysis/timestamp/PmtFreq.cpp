#include "../../CoreFuns.cpp"


void PmtFreq() {

    TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
    //TString fileName = "../../datafiles/FilteredForTot.root";
    UInt_t maxTime = 1; UInt_t focus = 0;
    TString pomId_picked = "162689671";
    UInt_t intPomId = 162689671;
    TString title = "Channel signal Frequency for plane " + pomId_picked;

    TCanvas *c = new TCanvas("c", "c", 1000, 500);
    TFile* f = new TFile(fileName);
    TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

    // variables to store data from root tree
    UInt_t timeStamp_s = 0;
    UInt_t timeStamp_ns = 0;
    UInt_t pomId = 0;
    UChar_t channel = 0;
    Char_t tot = 0;

    // assign branchs to variables
    signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
    signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
    signalTree->SetBranchAddress("PomId",& pomId);
    signalTree->SetBranchAddress("Channel",& channel);
    signalTree->SetBranchAddress("ToT",& tot);
  
    // Define the offfset so numbers arent too large for int
    signalTree->GetEntry(0);
    Double_t offset = timeStamp_s;
    
    Double_t maxTime_ns = maxTime * s_To_ns; // convert to ns
  
    TH1F *histo = new TH1F("histo", title , 30, 0, 30);
    histo->SetTitle(title + "; Channel; Signal Frequency");

    UInt_t signalIndex = 0;
    Double_t signalTime = 0;
    Double_t int_time;

    while (signalTime < maxTime_ns) {

      signalTree->GetEntry(signalIndex); 
      signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

      if (pomId == intPomId) {
	std::cout << (int) channel << std::endl;
	histo->Fill((int) channel);
      }
     
      signalIndex++;
    }
        
    gStyle->SetOptStat(0);
    gStyle->SetTitleFontSize(.09);
    gStyle->SetLabelSize(0.05, "XY");
    gStyle->SetTitleSize(0.05,"XY");
    gStyle->SetTitleOffset(.9,"X");
    gStyle->SetTitleOffset(.9,"Y"); 
   
    histo->Draw();
    histo->UseCurrentStyle();
}
