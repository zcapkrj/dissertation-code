/*
takes a file and finds the time between each pair of hits
*/

#include "../../CoreFuns.cpp"


// get all the histos from file and store in histos array
void TimeBetweenHits() {
  
  //TString fileName = "../../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  TString fileName = "../../datafiles/FilteredData.root";
 
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  double signalTime = 0;
  double nextSignalTime = 0;
  double diff = 0;
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  int maxIndex = signalTree->GetEntries();

  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH1F *histo = new TH1F("histo", "histo", 60, 0, 60);
  histo->SetTitle("Time between hits; Time Between Subsequent Hits/ns; Frequency");

  // loop while there is still a signal to check
  for (int i = 0; i <  maxIndex - 1; i++) {
    
    signalTree->GetEntry(i); // locate data in tree
    signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
    
    signalTree->GetEntry(i+1); // locate next data in tree
    nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset); // set time of next signal

    diff = nextSignalTime - signalTime;

    histo->Fill(diff);
  }

  histo->SetLineWidth(4);

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.06);
  gStyle->SetLabelSize(0.05, "XY");
  gStyle->SetTitleSize(0.05,"XY");
  gStyle->SetTitleOffset(.95,"X");
  gStyle->SetTitleOffset(1,"Y");

  
  histo->Draw();
  histo->UseCurrentStyle();
  
}
