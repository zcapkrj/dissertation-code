/* 
analyising random signals
*/

#include <TRandom.h>

void FlatDist() {
  
  TCanvas *c = new TCanvas("c", "c", 1000, 600); 
  TH1F *histo = new TH1F("histo", "Event Signals", 10000000, 0, 10000000);
  histo->SetTitle("Random events; Signals in Event");

  TCanvas *c2 = new TCanvas("c2", "c2", 1000, 600);  
  TH1F *histo2 = new TH1F("histo2", "Event Signals", 500, 0, 500);
  histo2->SetTitle("Time between Random events; Time between Subsequent Events");

  int N = 100000; // number of entries
  TRandom r;
  double holder;
  
  for (int i=0;i<N;i++) {
    holder = r.Rndm() * 10000000;

    histo->Fill(holder);
   }

  int index;
  double initial;
  double next;
  double diff;
  
  for (int i=0; i<10000000; i++) {

    initial =  histo->GetBinContent(i); // the initial hit 
    index = i; // index that can be change to find next hit
   
    if (initial > 0) {

      next = 0; // 

      while (next == 0) {
	index++;
	next = histo->GetBinContent(index);
      }
      
      diff = index - i;
      histo2->Fill(diff);

    }
  }

  c->cd();
  histo->Draw();

  c2->cd();
  histo2->Draw();
}
