/*
takes cosmic file and filters the tot between two values
*/

#include "../CoreFuns.cpp"

void CreateFilteredTotFile(TString fileName, TString newFileName, UInt_t upperTot, UInt_t lowerTot);

void JustTot() {
  
  TString fileName = "../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  CreateFilteredTotFile(fileName, "FilteredJustForTot.root", 31, 999);

}


void CreateFilteredTotFile(TString fileName, TString newFileName, UInt_t lowerTot, UInt_t upperTot) {
  std::cout << std::setprecision(15) << std::endl; // increase print precision for debugging
  
  //Get old file, old tree and set top branch address
  TFile* oldf = new TFile(fileName);
  TTree* oldTree = (TTree*) oldf->Get("CLBOpt_tree");
  Int_t nEntries = (Int_t) oldTree->GetEntries();

  // variables to store data from root tree
  UInt_t pomId = 0;
  UChar_t channel = 0;
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  Char_t tot = 0;
  
  // assign branchs of old file variables
  oldTree->SetBranchAddress("PomId",& pomId);
  oldTree->SetBranchAddress("Channel",& channel);
  oldTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  oldTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  oldTree->SetBranchAddress("ToT",& tot);

  //Create a new file + a clone of old tree in new file
  TFile *newFile = new TFile("../datafiles/" + newFileName,"recreate");
  TTree *newTree = oldTree->CloneTree(0);
  
  UInt_t intTot; // hold tot to check if in range
  UInt_t count = 0;

  for (int i = 0; i < nEntries; i++) {
    oldTree->GetEntry(i); // get hit from old tree
    intTot  = (int) tot;

    if ( (lowerTot < intTot) && (intTot < upperTot) ) {  // if tot within range save hit into new file
      count++;
      newTree->Fill();      
    }
  }

  std::cout << count << std::endl;
  //save and close file
  newTree->Write();
  newFile->Close();
  oldf->Close();
}
