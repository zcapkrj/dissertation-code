#include "../CoreFuns.cpp"


// assigns to array the cable length of every veto pmt (array index is the pmt pos)
void GetCableLengths(TString fileName, std::vector<double> * cableLengths) {
  ifstream myfile (fileName);
  
  std::string line; // varible to hold each line 
  double length;
  
  if (myfile.is_open()) {
    while ( getline(myfile,line) ) {// read every line of file
      
      length = (std::stod(line));
      cableLengths->push_back(length / 100); // convert to m and append to vecotr 
    }
  }
  else {
    std::cout << fileName + " not open" << std::endl;
  }

  
  myfile.close();
}


// get the channel pos 
Double_t GetPmtPos(TString pos, UInt_t pomId, UInt_t channel) {

  TString id = ToString(pomId); // convert pomId to string
  int pmtPos = 999;
  
  ifstream myfile (pos + "/datafiles/pomPmtPositionMap.txt"); // open config file
  std::string line; // varible to hold each line

  if (myfile.is_open()) { // if file was opened


    while ( getline(myfile, line) ) { // read every line of file 

      if ( line.find(id) != std::string::npos ) { // check for right pomid

	std::istringstream s(line); // stream for line
	std::vector<string> elements; // variable to save each element of line
	string element; // hold each element of line 
  
	while (s >> element) {
	  elements.push_back(element); // add element to end of elements vector
	}

	pmtPos = std::stod(elements[5 + channel]); // assign pmt pos 
      }
    }
  }
  else {
    std::cout << "pmt pos file not open" << std::endl;
  }
  
  myfile.close();
  return pmtPos;
}


Double_t CalcTimeAdjustmnet (Double_t pmtPos, std::vector<double> cableLengths) {
  
  return (cableLengths[pmtPos - 1] / cable_c) * s_To_ns;
}
