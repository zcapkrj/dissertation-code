/*
given a file this function gets each signal to check if they are in time order
 */

#include "../CoreFuns.cpp"

// declare functions
void CheckOrder(TString fileName);


void FileOrderCheck() {
  
  TString fileName = "../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  CheckOrder(fileName); // get and store all the events in events list
 
}

// get all the histos from file and store in histos array
void CheckOrder(TString fileName) {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  // init time holders 
  double signalTime = 0;
  double nextSignalTime = 0;
  
  // define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  int maxIndex = signalTree->GetEntries();
  
  // loop while there is still a signal to check
  for (int i=0; i<100; i++) {

    signalTree->GetEntry(i); // get first signal
    signalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
    std::cout << timeStamp_ns << std::endl;

    signalTree->GetEntry(i+1); // locate next data in tree
    nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

    if (isgreaterequal(signalTime, nextSignalTime)) { // check if first signal after next signal
      std::cout << "not in order" << std::endl;
    }
    else {
      std::cout << "in order" << std::endl;
    }
  }
}
