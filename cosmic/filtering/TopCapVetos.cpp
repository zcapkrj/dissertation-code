/*
takes cosmic file and creates a new file of all the top cap vetos adjusted for the cable length
*/

#include "AdjustmentFuns.cpp"

void CreateVetoFile(TString fileName, TString newFileName);

void TopCapVetos() {
  
  TString fileName = "../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  CreateVetoFile(fileName, "TopCapData.root");

}


void CreateVetoFile(TString fileName, TString newFileName) {
  std::cout << std::setprecision(15) << std::endl; // increase print precision for debugging
  
  //Get old file, old tree and set top branch address
  TFile* oldf = new TFile(fileName);
  TTree* oldTree = (TTree*) oldf->Get("CLBOpt_tree");
  Int_t nEntries = (Int_t) oldTree->GetEntries();

  // variables to store data from root tree
  UInt_t pomId = 0;
  UChar_t channel = 0;
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  Char_t tot = 0;
  
  // assign branchs of old file variables
  oldTree->SetBranchAddress("PomId",& pomId);
  oldTree->SetBranchAddress("Channel",& channel);
  oldTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  oldTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  oldTree->SetBranchAddress("ToT",& tot);

  //Create a new file + a clone of old tree in new file
  TFile *newFile = new TFile("../datafiles/" + newFileName,"recreate");
  TTree *newTree = oldTree->CloneTree(0);
  
  // Define the offfset so numbers arent too large for int
  oldTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // get all the planes that have convig data
  std::vector<UInt_t> tcPlanes;
  GetPlanes(& tcPlanes, 0, "../");

  // get cable lengths for high density planes 
  std::vector<double> cableLengths;
  GetCableLengths("../datafiles/tc_veto_cable_lengths", & cableLengths);

  Double_t pmtPos; // hold tot to check if in range
  UInt_t timeAdjust;
  
  for (int i = 0; i < nEntries; i++) {
    std::cout << i << std::endl;
    oldTree->GetEntry(i); // get hit from old tree

    if(std::find(tcPlanes.begin(), tcPlanes.end(), pomId) != tcPlanes.end()) { // check if tc plane

      pmtPos = GetPmtPos(pomId, channel); // get the pmt pos of the channel 
      
      if ( pmtPos < 0 ) { // check if a veto
	
	
	timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(pomId, channel), cableLengths); // get time adjustment

	if (timeAdjust > timeStamp_ns) { // checks if need to reduce second by 1
	  
	  timeStamp_s--;
	  timeStamp_ns = 1*s_To_ns - (timeAdjust - timeStamp_ns); // find new timeStamp_ns
	}
	else { // just reduce ns part if not
	  
	  timeStamp_ns -= timeAdjust;
	}
	
	
	newTree->Fill();
      }
    }
  }
  
  //save and close file
  newTree->Write();
  newFile->Close();
  oldf->Close();
}
