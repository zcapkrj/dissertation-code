/*
takes cosmic file and filters the tot between two values
*/

#include "AdjustmentFuns.cpp"

void CreateFilteredFile(TString fileName, TString newFileName, UInt_t upperTot, UInt_t lowerTot);

void CreateTotFile() {
  
  TString fileName = "../datafiles/type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
  CreateFilteredFile(fileName, "FilteredData2.root", 30, 100);

}


void CreateFilteredFile(TString fileName, TString newFileName, UInt_t lowerTot, UInt_t upperTot) {
  std::cout << std::setprecision(15) << std::endl; // increase print precision for debugging
  
  //Get old file, old tree and set top branch address
  TFile* oldf = new TFile(fileName);
  TTree* oldTree = (TTree*) oldf->Get("CLBOpt_tree");
  Int_t nEntries = (Int_t) oldTree->GetEntries();

  // variables to store data from root tree
  UInt_t pomId = 0;
  UChar_t channel = 0;
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  Char_t tot = 0;
  
  // assign branchs of old file variables
  oldTree->SetBranchAddress("PomId",& pomId);
  oldTree->SetBranchAddress("Channel",& channel);
  oldTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  oldTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  oldTree->SetBranchAddress("ToT",& tot);

  //Create a new file + a clone of old tree in new file
  TFile *newFile = new TFile("../datafiles/" + newFileName,"recreate");
  TTree *newTree = oldTree->CloneTree(0);
  
  // Define the offfset so numbers arent too large for int
  oldTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  // get all the planes that have convig data
  std::vector<UInt_t> bcPlanes;
  GetPlanes(& bcPlanes, 2, "../");

  // get cable lengths for high density planes 
  std::vector<double> cableLengths;
  GetCableLengths("../datafiles/cable_lengths.txt", & cableLengths);

  Double_t intTot; // hold tot to check if in range
  UInt_t timeAdjust;
  
  for (int i = 0; i < nEntries; i++) {
    
    oldTree->GetEntry(i); // get hit from old tree

    // remove the large noise pmts (add other bracket)
    if  ( (pomId != 162678077 && channel != 2) && (pomId != 16268300 && channel != 24) ) {
    
      intTot  = (int) tot;

      if ( (lowerTot < intTot) && (intTot < upperTot) ) {  // if tot within range 
	if(std::find(bcPlanes.begin(), bcPlanes.end(), pomId) != bcPlanes.end()) { // check if bc plane

	  timeAdjust = (int) CalcTimeAdjustmnet (GetPmtPos(pomId, channel), cableLengths); // get time adjustment

	  if (timeAdjust > timeStamp_ns) { // checks if need to reduce second by 1
	    timeStamp_s--;
	    timeStamp_ns = 1*s_To_ns - (timeAdjust - timeStamp_ns); // find new timeStamp_ns
	  }
	  else { // just reduce ns part if not
	    timeStamp_ns -= timeAdjust;
	  }

	  newTree->Fill();
	}
      }
    }
  }
  //save and close file
  newTree->Write();
  newFile->Close();
  oldf->Close();
}
