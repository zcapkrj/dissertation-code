// MAKE FLOW CHART FOR THIS IN DISS

// turn this into class with the different methods and plotting types

#include <list>
#include <string>
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TList.h>
#include <TF1.h>
#include <TPaveStats.h>

// global variables
TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
Double_t s_To_ns = 1000000000;

// represetns a single signal on a pmt
struct Signal {
  UInt_t pomId;
  UInt_t channel;
  Char_t tot;

  Signal(UInt_t getPomId, UInt_t getChannel, Char_t getTot) {
    pomId   = getPomId;
    channel = getChannel;
    tot     = getTot;
  }
};

// represents a collection of signals at a specific time
struct Event {
  Double_t timeOfEvent;
  std::list<Signal> signals;
};


TString ToString(int num);
void GetSignals(list<Event>& events);
void plotCluster(list<Event>& events);


void cluster() {

  std::list<Event> events;  // list to hold all the events

  GetSignals(events); // get and store all the events in events list
  
  plotCluster(events); // get all the histos and store them in histos array
}



// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// get all the histos from file and store in histos array
void GetSignals(list<Event>& events) {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;
  
  UInt_t window = 10; // size of signal window in 
  UInt_t signalIndex = 0; // index to hold signal index
  UInt_t signalCount; // amount of signals in event
  Double_t signalTime; // time of current signal
  Double_t nextSignalTime; // time of signal being check in range

  //UInt_t lastSignalIndex = signalTree->GetEntries();
  UInt_t lastSignalIndex = 500000;

  int groupCount = 0;

  // loop while there is still a signal to check
  while (signalIndex < lastSignalIndex) {

    signalTree->GetEntry(signalIndex); // locate data in tree
    signalTime = (timeStamp_s - offset) * s_To_ns + timeStamp_ns; // set time of first signal
    //std::cout << std::setprecision(15) << std::endl;
    //    std::cout << signalTime << std::endl;

    signalCount = 1; // set count to 1 for first signal
    signalIndex++; // increment index
    signalTree->GetEntry(signalIndex); // locate next data in tree
    
    nextSignalTime = (timeStamp_s - offset) * s_To_ns + timeStamp_ns; // set time of next signal
    
    // while next signal is within window of orginal keep adding more
    while (nextSignalTime - signalTime <= window) {
      signalCount++;

      // index is 1 behind the count so if larger than window index is still one after the last count
      signalIndex++;
      signalTree->GetEntry(signalIndex);
      nextSignalTime = (timeStamp_s - offset) * s_To_ns + timeStamp_ns; // set time of next signal
    }


    if (signalCount >= 10) { // check if enough signals to be an event and create Event
      //groupCount++;
      //std::cout<< groupCount << std::endl;
      Event event;
      
      for (int i=signalIndex-signalCount; i<signalIndex; i++) {
	
 	signalTree->GetEntry(i); // locate data in tree
	Signal signal(pomId, channel, tot);
	event.signals.push_back(signal);
      }

      // set time of event (average of first time and second time)
      event.timeOfEvent = (nextSignalTime + signalTime) / 2;
      //std::cout << event.timeOfEvent << std::endl;
      // add event to evetns list
      events.push_back(event);
    }
    else { // restart signal index to one after one just tested
      signalIndex = signalIndex - signalCount + 1; // move index to one after orginal
    }
  }
}


void plotCluster(list<Event>& events) {
    // define canvas for the ratio of ToT peaks
  TCanvas *c = new TCanvas("c", "c", 1000, 600);

  // create histograms for ratios and ADC of peaks
  TH1F *histo = new TH1F("histo", "Event Signals", 30000000, 0, 300000000000);

  // setting the title and axis labels
  histo->SetTitle("Event Signals non over lapping bins; Time/ns; Signals in Event");

  list<Event>::iterator it;
  for (it = events.begin(); it != events.end(); it++)
    {
      // Access the object through iterator
      Double_t time = (UInt_t) ((it->timeOfEvent) / 10000);

      std::cout<< time << std::endl;
      // UInt_t time = it->timeOfEvent;
      UInt_t eventSize = it->signals.size();
	
      histo->SetBinContent(time, eventSize);
}
  histo->SetStats(111);
  histo->Draw();
}
