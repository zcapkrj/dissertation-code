// MAKE FLOW CHART FOR THIS IN DISS

// using the variable clustering method for grouping hits together and forming events -> made a slide explaining this method, will try to remember to include in git file or email me zcapkrj@ucl.ac.uk

#include "../CoreFuns.cpp"

// init functions
void GetEvents(TString fileName, std::vector<Event>& events, Double_t width, Double_t size);
void plotCluster(std::vector<Event>& events, Double_t maxTimem, Double_t focus);


void ClusterVariable() {
  
  TString fileName = "../datafiles/FilteredBC.root";

  Double_t maxTime = 2; // max that that will be plotted
  Double_t focus = 1; // what second to focus on
  Double_t width = 10; // size of bin for checking
  Double_t size = 10; // amount of signals to be considered a bin
  std::vector<Event> events;  // list to hold all the events
  
  GetEvents(fileName, events, width, size); // get and store all the events in events list
  plotCluster(events, maxTime, focus); // get all the histos and store them in histos array

  std::cout << events.size() << std::endl;
}

void GetEvents(TString fileName, std::vector<Event>& events, Double_t width, Double_t size) {
  std::cout << std::setprecision(15) << std::endl;
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;
  
  Double_t signalTime = 0; // time of current signal
  Double_t nextSignalTime = 0; // time of signal being check in range
  UInt_t signalIndex = 0; // place holder for within the for loop
  UInt_t lastSignalIndex = signalTree->GetEntries();
  
  for (UInt_t i = 0; i < lastSignalIndex; i++) {

    //std::cout << i << std::endl;
    
    // initialise starting conditions
    signalIndex = i; // place holder so for loop count (i) doesnt need to change 
    Event event;

    signalTree->GetEntry(signalIndex); // get first hit entry 
    signalTime = nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset); 
    
    // check if next signal within window, if so add to event object and get next signal
    while ( ((nextSignalTime - signalTime) < width) && (signalIndex < lastSignalIndex) ) {

      signalTime = nextSignalTime; // update signal time
      
      Signal signal(pomId, channel, tot, signalTime); // create signal object for hit
      event.signals.push_back(signal); // add signal to event

      // get next hit
      signalIndex++;
      signalTree->GetEntry(signalIndex);
      nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
    }

    if (event.signals.size() > size) { // if size of event large enough add to events array

      event.timeOfEvent = signalTime; // time of event is the last signal time

      events.push_back(event);
      i = signalIndex; // start at last hit that didnt make the cut
    }
    
    else { // if not enough signals to form a hit got back to 1 after orginal index 
      i++;
    }
  }

  std::cout << events.size() << std::endl;
}

  
void plotCluster(vector<Event>& events, Double_t maxTime, Double_t focus) {
  std::cout << std::setprecision(15) << std::endl;
  
  TCanvas *c = new TCanvas("c", "c", 1500, 300);
  TH1F *histo = new TH1F("histo", "Event Signals", maxTime * s_To_ns / 1000, 0, maxTime * s_To_ns); // note divied by 1000 as cant have bins for ever ns as too many 
  histo->SetTitle("Event Signals using Variable Bins (10ns width); Time/s; Signals in Event");

  Double_t time;
  UInt_t eventSize;
    
  vector<Event>::iterator it;
  for (it = events.begin(); it != events.end(); it++) {

    // Access the object through iterator
    time = (UInt_t) ((it->timeOfEvent) / 1000);
    eventSize = it->signals.size();
	
    histo->SetBinContent(time, eventSize);
      
    //std::cout << time << std::endl;
}

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.1);
  gStyle->SetLabelSize(0.08, "XY");
  gStyle->SetTitleSize(0.1,"XY");
  gStyle->SetTitleOffset(0.4,"X");
  gStyle->SetTitleOffset(.2,"Y");

  
  histo->GetXaxis()->SetRangeUser(focus * s_To_ns, (focus + 0.011) * s_To_ns); // focus on specific second
  histo->Draw();
  histo->GetXaxis()->SetLimits(0, maxTime); // convert x axis to seconds
  histo->UseCurrentStyle();
}
