/* core functions used across all cosmic files */

// packegs needed
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TList.h>
#include <TF1.h>
#include <TPaveStats.h>
#include <math.h>
#include <string>

TString ToString(int num);
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset);


//global consts
const double s_To_ns = 1000000000;
const double c = 299792458;
const double air_c = 299704644.54;
const double cable_c = c * 0.64;


// represetns a single signal on a pmt
struct Signal {
  UInt_t pomId;
  UInt_t channel;
  Char_t tot;
  Double_t signalTime;

  Signal() {
    pomId = 0;
    channel = 0;
    tot = 0;
    signalTime = 0;
  }

  Signal(UInt_t getPomId, UInt_t getChannel, Char_t getTot, Double_t getTime) {
    pomId   = getPomId;
    channel = getChannel;
    tot     = getTot;
    signalTime = getTime;
  }
};

// represents a collection of signals at a specific time
struct Event {
  Double_t timeOfEvent;
  std::vector<Signal> signals;
};


// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// function takes the times from the root file and combines them for the ns timestamp
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset) {
  
  return (timeStamp_s - offset) * s_To_ns + timeStamp_ns;
}


// gets all the bottom cap planes from the convig file, used to filter out any planes with not data
void GetPlanes(std::vector<UInt_t> * planes, UInt_t type, TString location) {
  
  ifstream myfile (location + "datafiles/pomPmtPositionMap.txt"); // open config file
  std::string line; // varible to hold each line

  // initialise signal variables

  UInt_t pomId;
  UInt_t type_index; 

  if (myfile.is_open()) { // if file was opened

    while ( getline(myfile, line) ) { // read every line of file 

      std::istringstream s(line); // stream for line
      std::vector<string> elements; // variable to hold each element of line
      string element; // hold each element of line 

      while (s >> element) {
	elements.push_back(element); // add element to end of elements vector
      }

      pomId      = std::stoi(elements[4]);
      type_index = std::stoi(elements[1]);
      
      if (type_index == type) {

	planes->push_back(pomId);
      }
    }
  }
  else {
    std::cout << "pomPmtPositionMap.txt didn't open" << std::endl;
  }

  myfile.close();
}


vector<Double_t> GetPlanesInData (TString fileName) {
  
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t pomId = 0;
  signalTree->SetBranchAddress("PomId",& pomId);
  
  UInt_t nEntries = signalTree->GetEntries();
  std::vector<Double_t> planes;

  for (int i = 0; i < nEntries; i++) {

    signalTree->GetEntry(i); // locate data in tree

    if( std::find(planes.begin(), planes.end(), pomId) == planes.end() ) {
      
      planes.push_back(pomId); 
    }
  }

  return planes;
}

