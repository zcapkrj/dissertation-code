#include <TROOT.h>

TString fileName = "copied_file.root";

void CreateTotFile() {

  //Get old file, old tree and set top branch address
  TFile* oldf = new TFile(fileName);
  TTree* oldTree = (TTree*) oldf->Get("CLBOpt_tree");
  Int_t nEntries = (Int_t)oldTree->GetEntries();

  // variables to store data from root tree
  UInt_t pomId = 0;
  UChar_t channel = 0;
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  Char_t tot = 0;
  
  // assign branchs of old file variables
  oldTree->SetBranchAddress("PomId",& pomId);
  oldTree->SetBranchAddress("Channel",& channel);
  oldTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  oldTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  oldTree->SetBranchAddress("ToT",& tot);


   //Create a new file + a clone of old tree in new file
   TFile *newFile = new TFile("FilteredTot.root","recreate");
   TTree *newTree = oldTree->CloneTree(0);
   
   nEntries = 1000;
   Double_t int_tot; // hold tot to check
  
  for (int i = 0; i < nEntries; i++) {
    oldTree->GetEntry(i);
    int_tot  = (int) tot;
    
    if (40 < int_tot && int_tot < 70) {
      // if tot within range save hit into new file

      newTree->Fill();
    }
  }

  newTree->Write();
  newFile->Close();
}
