// MAKE FLOW CHART FOR THIS IN DISS

// turn this into class with the different methods and plotting types

#include <list>
#include <string>
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TList.h>
#include <TF1.h>
#include <TPaveStats.h>

// global variables
TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";
Double_t s_To_ns = 1000000000;

// represetns a single signal on a pmt
struct Signal {
  UInt_t pomId;
  UInt_t channel;
  Char_t tot;

  Signal(UInt_t getPomId, UInt_t getChannel, Char_t getTot) {
    pomId   = getPomId;
    channel = getChannel;
    tot     = getTot;
  }
};

// represents a collection of signals at a specific time
struct Event {
  Double_t timeOfEvent;
  std::list<Signal> signals;
};


// init functions
TString ToString(int num);
void GetSignals(list<Event>& events);
void plotCluster(list<Event>& events);
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset);


void clusterNonoverlap() {

  std::list<Event> events;  // list to hold all the events

  GetSignals(events); // get and store all the events in events list
  
  plotCluster(events); // get all the histos and store them in histos array
}



// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}

// function takes the times from the root file and combines them for the ns timestamp
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset) {
  
  return (timeStamp_s - offset) * s_To_ns + timeStamp_ns;
}

// get all the histos from file and store in histos array
void GetSignals(list<Event>& events) {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);
  
  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;
  
  UInt_t window = 10; // size of signal window in 
  Double_t signalTime; // time of current signal
  Double_t nextSignalTime; // time of signal being check in range
  UInt_t signalIndex; // place holder for within the for loop
  //UInt_t lastSignalIndex = signalTree->GetEntries();
  UInt_t lastSignalIndex = 10000000;

  
  for (UInt_t i = 0; i < lastSignalIndex;) {
    signalIndex = i;
    
    Event event; // init event struct
    
    signalTree->GetEntry(signalIndex); // locate first data point
    signalTime = nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset); // set time of first and next to the first entry

    Signal signal(pomId, channel, tot);
    event.signals.push_back(signal); // add first signal to event

    while ((nextSignalTime - signalTime) < window) {
    
      // if while condition true then add next signal to event
      Signal signal(pomId, channel, tot);
      event.signals.push_back(signal);

      // now get the next signal that will be checked by the while loop
      signalIndex++;
      signalTree->GetEntry(signalIndex);
      nextSignalTime = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);     
    }

    if (event.signals.size() > 60) { // if size of event large enough add to events array
      event.timeOfEvent = signalTime;
      //event.timeOfEvent = (nextSignalTime + signalTime) / 2;
      events.push_back(event);

      //std::cout << i << std::endl;
      //std::cout << event.signals.size() << std::endl;
      
      i += (event.signals.size());

      //std::cout << i << std::endl;
    }
    else { // if not an event then increment i by 1
      
      i++;
      }
  }
}

  
void plotCluster(list<Event>& events) {
    // define canvas for the ratio of ToT peaks
  TCanvas *c = new TCanvas("c", "c", 1000, 600);

  // create histograms for ratios and ADC of peaks
  TH1F *histo = new TH1F("histo", "Event Signals", 300000000, 0, 300000000000);

  // setting the title and axis labels
  histo->SetTitle("Event Signals non over lapping bins; Time/ns; Signals in Event");

  list<Event>::iterator it;
  for (it = events.begin(); it != events.end(); it++)
    {
      // Access the object through iterator
      Double_t time = (UInt_t) ((it->timeOfEvent) / 1000);

      //std::cout << setPrecision(15) << std::endl;
      //std::cout<< time / 1000 << std::endl;
      // UInt_t time = it->timeOfEvent;
      UInt_t eventSize = it->signals.size();

      //std::cout << eventSize << std::endl;
	
      histo->SetBinContent(time, eventSize);
}
  histo->SetStats(111);
  histo->Draw();
}
