#include <string>
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TList.h>
#include <TF1.h>
#include <TPaveStats.h>

// global variables
int numOfHistos = 30; // find way of getting this from file
UInt_t pomIDSelection = 162678077; // plane id
TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";

int epoc_adjust = 1546300800;
int ns_in_s = 10;

// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// get all the histos from file and store in histos array
void unwrapChannel() {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signal_tree = (TTree*) f->Get("CLBOpt_tree");

  TCanvas *c = new TCanvas("c", "canvas", 1000, 1000);

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomID = 0;
  UChar_t channel = 0;
  TTimeStamp time; // time object 

  // assign branchs to variables
  signal_tree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signal_tree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signal_tree->SetBranchAddress("PomId",& pomID);
  signal_tree->SetBranchAddress("Channel",& channel);

  // Define the lowest histogram limit 
  TDatime T1(2019,12,6,11,28,00);
  Double_t start = T1.Convert();

  // Define the hightest histogram limit
  TDatime T2(2019,12,06,11,35,00);
  Double_t end = T2.Convert();

  TString histoname = ToString(pomIDSelection) + " Channel 0 TimeStamp"; 
  TH1D* h1 = new TH1D(histoname, histoname, end-start, start, end);

  // loop through all planes
  // loop through all the channels (focusing on 1 channel of plane rn)

  // loop through all the data and add the correct data to the hist
  for(int i=0; i<signal_tree->GetEntries(); ++i){
    signal_tree->GetEntry(i);
    
    if (pomID == pomIDSelection && channel == 0) {
      time = TTimeStamp(timeStamp_s, timeStamp_ns); // time object
      std::cout<< time << std::endl;
      h1->Fill(timeStamp_s);
    }
  }

  h1->GetXaxis()->SetTimeDisplay(1); 
  h1->GetXaxis()->SetLabelSize(0.03);
  h1->GetXaxis()->SetTimeFormat("%H:%M");
  h1->Draw();
}


void cosmic_analysis() {

  unwrapChannel(); // get all the histos and store them in histos array
}
