#include <cmath> 

#include <TROOT.h>
#include <list>


// represetns a single signal on a pmt
struct Signal {
  UInt_t pomId;
  UInt_t channel;
  Double_t tot;
  UInt_t time;
  double s_To_ns = 1000000000;

  Signal() {
    tot = 0;
  }

  Signal(UInt_t getPomId, UInt_t getChannel, Char_t getTot, Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset) {
    pomId   = getPomId;
    channel = getChannel;
    tot     = (int) getTot;
    time    = (timeStamp_s - offset) * s_To_ns + timeStamp_ns;
  }
};


//void PlotEvent(TH2F *histo, Event event);
TString ToString(int num);
void GetPMTx(float & x, int pmtPos, int _phx, int _phy);
void GetPMTy (float & y, int _pomType, int pmtPos, int _phx, int _phy);
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset);
void getXYcoords(std::string line, int channel, float & x, float & y);
bool IsBeside(Signal signal1, Signal signal2);

//TString fileName = "FilteredToT.root";
TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";

double s_To_ns = 1000000000;


// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// function takes the times from the root file and combines them for the ns timestamp
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset) {
  
  return (timeStamp_s - offset) * s_To_ns + timeStamp_ns;
}


void TimeAndTotPosFilter() {
  
    // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  TCanvas *c = new TCanvas("c", "c", 1000, 600);
  TH2F *histo = new TH2F("histo", "histo", 300, 0, 300, 19, 31, 50);
  histo->SetTitle("Time between hits and ToT (2); Time Between Subsequent Hits/ns; ToT");

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomId = 0;
  UChar_t channel = 0;
  Char_t tot = 0;

  std::cout << "done" << std::endl;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomId);
  signalTree->SetBranchAddress("Channel",& channel);
  signalTree->SetBranchAddress("ToT",& tot);

  double signalTime = 0;
  double nextSignalTime = 0;
  double diff = 0;
  bool isBeside;

  // Define the offfset so numbers arent too large for int
  signalTree->GetEntry(0);
  Double_t offset = timeStamp_s;

  int maxIndex = signalTree->GetEntries() - 1;
  //maxIndex = 10000000;

  // loop while there is still a signal to check
  for (int i = 0; i < 1000;) {

    isBeside = false;
    
    signalTree->GetEntry(i); // locate data in tree
    Signal signal1(pomId, channel, (int) tot, timeStamp_s, timeStamp_ns, offset);

    if (signal1.tot > 30 && isBeside == false) { // check if first signal greater than 30

      Signal signal2 = signal1; // initialise signal2 
      signal2.tot = 0; // set tot to 0 so will defs lopp once

      while (signal2.tot < 31 && isBeside == false) { // add beside check and greate thab check
	i++;
	signalTree->GetEntry(i);
	signal2.pomId = pomId;
	signal2.channel = channel;
	signal2.tot = (int) tot;
	signal2.time = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);

	isBeside = IsBeside(signal1, signal2);
	
      }

      diff = signal2.time - signal1.time; // difference between two times
      histo->Fill(diff, signal1.tot);
    }
    else {
      i++;
    }
  }

  //int i = histo->GetBinContent();

  histo->SetStats(0);
  histo->GetZaxis()->SetRangeUser(0, 500);
  histo->Draw("COLZ");
  //histo->GetXaxis()->SetLimits(0, 5);
}


bool IsBeside(Signal signal1, Signal signal2) {
  
  TString id1 = ToString(signal1.pomId); // convert pomId to string
  TString id2 = ToString(signal2.pomId); // convert pomId to string
  float x1; float x2; float y1; float y2;
  
  ifstream myfile ("pomPmtPositionMap.txt"); // open config file
  std::string line; // varible to hold each line
    
  if (myfile.is_open()) { // if file was opened

    while ( getline(myfile, line) ) { // read every line of file 

      if ( line.find(id1) != std::string::npos) { // check if right pomid

	getXYcoords(line, signal1.channel, x1, y1);
      }

      if ( line.find(id2) != std::string::npos) { // check if right pomid

	getXYcoords(line, signal2.channel, x2, y2);
      }
    }
    myfile.close();

    if ( std::abs(x1-x2) == 1 ||  std::abs(y1-y2) ) {
      return true;
    }
    else {
      return false;
    }
  }

  return false;
}


void getXYcoords(std::string line, int channel, float & x, float & y) {
  
  std::istringstream s(line); // stream for line
  std::vector<string> elements; // variable to hold each element of line
  string element; // hold each element of line 
  
  while (s >> element) {
    elements.push_back(element); // add element to end of elements vector
  }

  int type_index           = std::stoi(elements[1]);
  int pom_x_position_index = std::stoi(elements[2]);
  int pom_y_position_index = std::stoi(elements[3]);
  int pmtPos               = std::stoi(elements[5 + channel]);
  
  GetPMTx(x, pmtPos, pom_x_position_index, pom_y_position_index);
  GetPMTy(y, type_index, pmtPos, pom_x_position_index, pom_y_position_index);
}


void  GetPMTx (float & x, int pmtPos, int _phx, int _phy) {

  x = -999;

  if (pmtPos >= 0) {
    x = -((_phx-1) * 5 +  ( (pmtPos - 1) % 5) + 1);
  }
}
 

void GetPMTy (float & y, int _pomType, int pmtPos, int _phx, int _phy) {
  
  y = -999;
  
  if(_pomType == 0 && pmtPos > 0) {
    y = ( 5*(8 - _phy-0.6 * abs(_phx-1)) -  ((pmtPos-1) / 5) );
  }
  else if(_pomType == 2 ) {
    y = (   6*( 7.8 - _phy -0.6 * abs(_phx-1) ) -  ((pmtPos-1)/5));  // 7.8 not correct 
  }
}
