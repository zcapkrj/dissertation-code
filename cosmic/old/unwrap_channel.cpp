#include <string>
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TList.h>
#include <TF1.h>
#include <TPaveStats.h>

// global variables
int numOfHistos = 30; // find way of getting this from file
UInt_t pomIDSelection = 162678077; // plane id
TString fileName = "type2_run_06_12_19_nominalV_threshold80_veto100_5mins.root";

UInt_t s_To_ns = 1000000000;

// function converts an int to TString
TString ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// get all the histos from file and store in histos array
void UnwrapChannel() {
  
  // load file and get tree
  TFile* f = new TFile(fileName);
  TTree* signalTree = (TTree*) f->Get("CLBOpt_tree");

  TCanvas *c = new TCanvas("c", "canvas", 1000, 1000);

  // variables to store data from root tree
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomID = 0;
  UChar_t channel = 0;

  // assign branchs to variables
  signalTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  signalTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  signalTree->SetBranchAddress("PomId",& pomID);
  signalTree->SetBranchAddress("Channel",& channel);

  std::cout << std::setprecision(10);
  
  // Define the lowest histogram limit
  signalTree->GetEntry(0);
  Double_t start  = 0;
  Double_t offset = timeStamp_s;
  
  // Define the hightest histogram limit
  signalTree->GetEntry(signalTree->GetEntries()-1);
  Double_t end = (timeStamp_s - offset) * s_To_ns + timeStamp_ns;

  TString histoname = ToString(pomIDSelection) + " Channel 0 TimeStamp"; 
  TH1D* h1 = new TH1D(histoname, histoname, 300, start, end);

  // loop through all planes
  // loop through all the channels (focusing on 1 channel of plane rn)
  // loop through all the data and add the correct data to the hist
  for(int i=0; i<signalTree->GetEntries(); ++i){
    signalTree->GetEntry(i);
    if (pomID == pomIDSelection && channel == 0) {
      h1->Fill((timeStamp_s - offset) * s_To_ns + timeStamp_ns);
    }
  }

  UInt_t signalIndex = 0; // index to hold signal index
  UInt_t signalTime; //
  UInt_t nextSignalTime;

  // loop while there is still a signal to check
  while (signalIndex < signalTree->GetEntries()) {
    signalTree->GetEntry(signalIndex); // locate data in tree
    signalTime = (timeStamp_s - offset) * s_To_ns + timeStamp_ns; // set time of signal
    

    
    
    
  }

  
  h1->Draw();
}

void unwrap_channel() {

  UnwrapChannel(); // get all the histos and store them in histos array
}
