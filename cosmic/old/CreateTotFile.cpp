#include <TROOT.h>

TString fileName = "copied_file.root";

void CreateTotFile() {

  // create new file and tree
  TFile *f = new TFile("FilteredToTs.root","RECREATE","Filtered ToT from cosmics");
  TTree *tree = new TTree("myTree","A ROOT tree");

  // variables to store data from root tree
  UInt_t pomIdNew = 0;
  UChar_t channelNew = 0;
  UInt_t timeStamp_sNew = 0;
  UInt_t timeStamp_nsNew = 0;
  Char_t totNew = 0;
  
  // create the branches for each variable of hit and assign variable to them
  TBranch *pomIdBr = tree->Branch("PomId", &pomIdNew, "PomId");
  TBranch *channelBr = tree->Branch("Channel", &channelNew, "Channel");
  TBranch *timeStamp_sBr = tree->Branch("TimeStamp_s", &timeStamp_sNew, "TimeStamp");
  TBranch *timeStamp_nsBr = tree->Branch("TimeStamp_ns", &timeStamp_nsNew, "TimeStamp");
  TBranch *totBr = tree->Branch("ToT",&totNew,"ToT");

  // load old file and get hit tree
  TFile* oldf = new TFile(fileName);
  TTree* hitTree = (TTree*) oldf->Get("CLBOpt_tree");

  // variables to store data from old root tree
  UInt_t pomId = 0; 
  UChar_t channel = 0;
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  Char_t tot = 0; 
  
  // assign branchs of old file variables
  hitTree->SetBranchAddress("PomId",& pomId);
  hitTree->SetBranchAddress("Channel",& channel);
  hitTree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  hitTree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  hitTree->SetBranchAddress("ToT",& tot);

  int maxIndex = hitTree->GetEntries();
  maxIndex = 1000;
  Double_t int_tot; // hold tot to check
  
  for (int i = 0; i < (maxIndex-1); i++) {
    hitTree->GetEntry(i);
    int_tot  = (int) tot;
    
    if (40 < int_tot && int_tot < 70) {
      // if tot within range save hit into new file

      pomIdNew = pomId;
      channelNew = channel;
      timeStamp_sNew = timeStamp_s;
      timeStamp_nsNew = timeStamp_ns;
      totNew = tot;
      
      pomIdBr->Fill();
      channelBr->Fill();
      timeStamp_sBr->Fill();
      timeStamp_nsBr->Fill();
      totBr->Fill();
    }
  }

  tree->Write();
  f->Close();
}
