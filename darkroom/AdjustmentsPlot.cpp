
#include "TimeAdjustments.cpp"


void AdjustmentsPlot() {


}


void InvestigateAdjustments() {
  
  int channel_pos;
  double unadjustedTimeStamps[channels];
  double adjustedTimeStamp[channels];
  double adjustedTimeStampDists[channels];
  double adjustedTimeStampCables[channels];
  double hist_rms[channels];
  
  // get the peak values of timestamp histoms
  for (int channel=0; channel<channels; channel++) {

    channel_pos = pmt_pos[channel] - 1;
    std::cout << channel_pos << std::endl;
    
    double originalTime = time_stamp_histos[channel]->GetMaximumBin();
    hist_rms[channel] = time_stamp_histos[channel]->GetRMS();

    unadjustedTimeStamps[channel] = originalTime;
    
    adjustedTimeStamp[channel] = originalTime - time_adjustments[channel];

    adjustedTimeStampDists[channel] = originalTime - (flasher_dist[channel_pos] / air_c) * s_To_ns;

    adjustedTimeStampCables[channel] = originalTime - (cable_lengths[channel_pos] / cable_c) * s_To_ns;

    std::cout << unadjustedTimeStamps[channel]  << std::endl;
    std::cout << adjustedTimeStamp[channel]  << std::endl;
    std::cout << adjustedTimeStampDists[channel]  << std::endl;
    std::cout << adjustedTimeStampCables[channel]  << std::endl;
    std::cout << ""  << std::endl;
    //std::cout << (flasher_dist[channel_pos] / air_c) << std::endl;
  }

  TCanvas *c4 = new TCanvas("c4", "c4", 1000, 500);

  TGraphErrors *gr1 = new TGraphErrors(channels, pmt_pos, unadjustedTimeStamps, 0, hist_rms);
  gr1->SetName("gr1");
  gr1->SetTitle("Unadjusted Time Stamps");
  gr1->SetMarkerStyle(21);
  gr1->SetMarkerColor(1);
  //gr1->SetFillStyle(0);

  TGraphErrors *gr2 = new TGraphErrors(channels, pmt_pos, adjustedTimeStamp, 0, hist_rms);
  gr2->SetName("gr2");
  gr2->SetTitle("Time Stamps Adjusted for Cables and Distances");
  gr2->SetMarkerStyle(22);
  gr2->SetMarkerColor(2);
  //gr2->SetFillStyle(0);

  TGraph *gr3 = new TGraph(channels, pmt_pos, adjustedTimeStampDists);
  gr3->SetName("gr3");
  gr3->SetTitle("Time Stamps Adjusted for Distances");
  gr3->SetMarkerStyle(23);
  gr3->SetMarkerColor(3);
  //gr3->SetFillStyle(0);

  TGraph *gr4 = new TGraph(channels, pmt_pos, adjustedTimeStampCables);
  gr4->SetName("gr4");
  gr4->SetTitle("Time Stamps Adjusted for Cables");
  gr4->SetMarkerStyle(24);
  gr4->SetMarkerColor(4);
  //gr3->SetFillStyle(0);
  
  TMultiGraph  *mg  = new TMultiGraph("mg", "mg");

  mg->SetTitle("Adjusted TimeStamps at PMT positions");
  mg->GetXaxis()->SetTitle("PMT Postion");
  mg->GetYaxis()->SetTitle("Time/ns");
  mg->GetYaxis()->SetLimits(140,200);
  
  mg->Add(gr1);
  mg->Add(gr2);
  mg->Add(gr3);
  mg->Add(gr4);

  gr3->Draw("ap");
  //mg->Add(gr4);
  mg->Draw("ap");
  c4->BuildLegend();
}
