#include "CoreFuns.cpp"

// need to add auto adjustment of max freq so fits better

void GetSingleTimeStamp(TString fileName, TH1F* h, TString pomId, TString channel);
void PlotSingleTimeStamp(TH1F* h, TCanvas* c);

void SingleTimeStamp() {

  TString pomId = "162666998";
  TString channel = "15";
  
  TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";
  TString histo_name = "TimeStamp of channel " + channel; // create histograms name
  TH1F* histo = new TH1F("h1", "TimeStamp of Channel " + channel, 500, 0, 500);
  GetSingleTimeStamp(fileName, histo, pomId, channel);

  TCanvas *c = new TCanvas("c", "canvas", 800, 800); // define the Canvas
  PlotSingleTimeStamp(histo, c);
}


void GetSingleTimeStamp(TString fileName, TH1F* h, TString pomId, TString channel) {

  //h = new TH1F("h1", "TimeStamp of Channel " + channel, 500, 0, 500);
  
  // open the file
  TFile *f = TFile::Open(fileName);
  TTree* tree = (TTree*) f->Get("CLBOpt_tree");

  // variables to store data from root tree
  UInt_t timeStamp = 0; // for combined timestamp
  UInt_t timeStamp_s = 0;
  UInt_t timeStamp_ns = 0;
  UInt_t pomIdHolder = 0;
  UChar_t channelHolder = 0;

  // assign branchs to variables
  tree->SetBranchAddress("TimeStamp_s",& timeStamp_s);
  tree->SetBranchAddress("TimeStamp_ns",& timeStamp_ns);
  tree->SetBranchAddress("PomId",& pomIdHolder);
  tree->SetBranchAddress("Channel",& channelHolder);

  // Define the offfset so numbers arent too large for int
  tree->GetEntry(0);
  Double_t offset = timeStamp_s;

  int maxIndex = tree->GetEntries();

  // start loop and create all the histograms
  for (int i = 0; i < maxIndex; i++) {

    tree->GetEntry(i);
    
    if ( (ToString(pomIdHolder) == pomId) & (ToString(channelHolder) == channel) ) {

      timeStamp = CombineTimeStamps(timeStamp_s, timeStamp_ns, offset);
      h->Fill(timeStamp % 50000); 
    }
  }
}


void PlotSingleTimeStamp(TH1F* h, TCanvas* c) {

  int maxBin; int lower; int upper; // vars to focus plot on peak
  maxBin = h->GetMaximumBin();
  lower = maxBin - 10;
  upper = maxBin + 10;
  h->GetXaxis()->SetRange(lower,upper);

  gStyle->SetOptStat(1);
  gStyle->SetTitleFontSize(.05);
  gStyle->SetLabelSize(0.03, "XY");
  gStyle->SetTitleSize(0.04,"XY");
  gStyle->SetTitleOffset(1,"X");
  gStyle->SetTitleOffset(1.2,"Y");

  h->SetLineWidth(4);  
  h->GetYaxis()->SetTitle("Frequency");
  h->GetXaxis()->SetTitle("TimeStamp/ns");
  h->Draw();
  h->UseCurrentStyle();
  //h->GetYaxis()->SetRangeUser(0, 1000);
}
