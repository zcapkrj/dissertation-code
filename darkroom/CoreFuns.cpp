#include <TROOT.h>
#include <iostream>
#include <sstream>

//global consts
const double s_To_ns = 1000000000;
const double c = 299792458;
const double air_c = 299704644.54;
const double cable_c = c * 0.64;

const UInt_t channels = 30; // im working with hd bc planes so 30 channels

// convert number to TString object
TString ToString(int num) {
  std::ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// function takes the times from the root file and combines them for the ns timestamp
Double_t CombineTimeStamps(Double_t timeStamp_s, Double_t timeStamp_ns, Double_t offset) {
  
  return (timeStamp_s - offset) * s_To_ns + timeStamp_ns;
}


// locate the pom number from the convig file (each convig file have multiple planes so need identifier)
TString GetPomIdNum(TString pomId) {
  
  std::ifstream config ("datafiles/config.txt"); // open config file
  
  std::string line; // varible to hold each line 
  std::size_t found; // variable for pos of equals sign
  TString pomNum; // variable to hold the pom number from the covig file 
  
  if (config.is_open()) {
    // read every line of file 
    while ( getline(config,line) ) {
      // check if right pomid and  line is the hv argument
      if (line.find(pomId) != std::string::npos) {
	found = line.find("pom"); 
	pomNum = (line.substr(found+3,1));
      }
    }
    config.close();
  }
  return pomNum;
}
