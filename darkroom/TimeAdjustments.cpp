// not working for last function

#include <TROOT.h>

#include "MultiTimeStamp.cpp"

// note using vectors as not compatible with TGraph

void GetPomNum(TString pomId, TString & pomNum);
void GetPmtPos(TString pomId, TString & pomNum, double * pmtPos);
void GetFlasherDist(TString pomId, double * flasherDist);
void GetCableLengths(TString pomId, double * cableLengths);

void CalcAdjustments(double * pmtPos,
		     double * flasherDist ,
		     double * cableLengths,
		     double * timeAdjustments,
		     double * flasherDistAdjustment,
		     double * cableLenAdjustment);

void plotAdjustments(double * pmtPos,
		     double * timeAdjustments,
		     double * flasherDistAdjustments,
		     double * cableLenAdjustments);

void InvestigateAdjustments(TH1F ** timeStampHisto,
			    double * pmtPos,
			    double * timeAdjustments,
			    double * flasherDistAdjustments,
			    double * cableLenAdjustments);


void TimeAdjustments() {
  
  double pmtPos[channels];
  double flasherDist[channels];
  double cableLengths[channels];
  double timeAdjustments[channels];
  double flasherDistAdjustments[channels];
  double cableLenAdjustments[channels];

  TString pomId = "162625384";
  TString pomNum = GetPomIdNum(pomId);
  
  GetPmtPos(pomId, pomNum, pmtPos);
  GetFlasherDist(pomId, flasherDist);
  GetCableLengths(pomId, cableLengths);
  CalcAdjustments(pmtPos, flasherDist, cableLengths, timeAdjustments, flasherDistAdjustments, cableLenAdjustments);

  //plotAdjustments(pmtPos, timeAdjustments, flasherDistAdjustments, cableLenAdjustments);

  TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";
  TH1F* timeStampHistos[channels];  

  GetTimeStamps(fileName, timeStampHistos, "162666998");
  
  InvestigateAdjustments(timeStampHistos,
			 pmtPos,
			 timeAdjustments,
			 flasherDistAdjustments,
			 cableLenAdjustments);
}


// assigns to array the pmt positions (array index as the channels)
void GetPmtPos(TString pomId, TString & pomNum, double * pmtPos) {
  
  ifstream myfile ("datafiles/config.txt"); // open config file
  
  std::string line; // varible to hold each line 
  std::size_t found; // variable for pos of equals sign
  double pos;
  int channel = 0; // channel count
  
  if (myfile.is_open()) {
    while ( getline(myfile,line) ) { // read every line of file 

      if (line.find(("pom" + pomNum)) != std::string::npos && // check if right pomid 
	  line.find("pos") != std::string::npos) { // and line is the hv variable
	
	found = line.find("="); // find equals sign on line
	pos = (std::stod(line.substr(found+1))); // convert pos to int and store

	if (channel < 30) { // make sure we dont go over the max channels (im obly using bottom cap to 30)
	  pmtPos[channel] = pos; // store in pmt array
	  channel++;
	}
      }
    }
    myfile.close();
  }  
}


// assigns to array the distance from pmt position to flasher  (array index is the pmt pos)
void GetFlasherDist(TString pomId, double * flasherDist) {
  
  ifstream myfile ("datafiles/flasher_dist.txt"); // open config file
  
  std::string line; // varible to hold each line 
  double dist;
  int position = 0; // channel count
  
  if (myfile.is_open()) {
    while ( getline(myfile,line) ) { // read every line of file
      
      dist = (std::stod(line.substr(line.length() - 3))); // distance is last 3 didgits of line in file
      flasherDist[position] = dist / 100; // convert to m
      position++;
    }
  }
  
  myfile.close();
}


// assigns to array the cable length of every pmt (array index is the pmt pos)
void GetCableLengths(TString pomId, double * cableLengths) {
  ifstream myfile ("datafiles/cable_lengths.txt");
  
  std::string line; // varible to hold each line 
  double length;
  int position = 0; // channel count
  
  if (myfile.is_open()) {
    while ( getline(myfile,line) ) {// read every line of file
      
      length = (std::stod(line.substr(line.length() - 3))); // length is last 3 digits of line
      cableLengths[position] = length / 100; // convert to m
      position++;
    }
  }
  myfile.close();
}


void CalcAdjustments(double * pmtPos,
		     double * flasherDist ,
		     double * cableLengths,
		     double * timeAdjustments,
		     double * flasherDistAdjustments,
		     double * cableLenAdjustments) {
  
  int channelPos; // pmt position for a channel holder

  for (int channel=0; channel < 30; channel++) { // set to 30 for bottom cap

    channelPos = pmtPos[channel] - 1; // get pmt position of channel (-1 so can be used in array)
	
    flasherDistAdjustments[channel] = (flasherDist[channelPos] / air_c) * s_To_ns;
    cableLenAdjustments[channel] = (cableLengths[channelPos] / cable_c) * s_To_ns;
    
    timeAdjustments[channel] = flasherDistAdjustments[channel] + cableLenAdjustments[channel]; // for time adjustment
  }
}



void plotAdjustments(double *  pmtPos,
		     double * timeAdjustments,
		     double * flasherDistAdjustments,
		     double * cableLenAdjustments) {

  // plot for both adjustments 
  TCanvas *c1 = new TCanvas("c1", "", 600, 600);
  TGraph *g1 = new TGraph(30, pmtPos, timeAdjustments); // 30 for bottom caps 
  g1->SetMarkerColor(4);
  g1->SetMarkerSize(0.5);
  g1->SetMarkerStyle(21);
  g1->SetTitle("Time adjustments due to cable lengths and distances");
  g1->GetXaxis()->SetTitle("PMT Postion");
  g1->GetYaxis()->SetTitle("Time Adjustment/ns");
  g1->Draw("ap");

  // just flasher distance
  TCanvas *c2 = new TCanvas("c2", "", 600, 600);
  TGraph *g2 = new TGraph(30, pmtPos, flasherDistAdjustments);
  g2->SetMarkerColor(4);
  g2->SetMarkerSize(0.5);
  g2->SetMarkerStyle(21);
  g2->SetTitle("Time adjustment at PMT positions due to flasher distances");
  g2->GetXaxis()->SetTitle("PMT Postion");
  g2->GetYaxis()->SetTitle("Time Adjustment/ns");
  g2->Draw("ap");

  // just cable lengths
  TCanvas *c3 = new TCanvas("c3", "", 600, 600);
  TGraph *g3 = new TGraph(30, pmtPos, cableLenAdjustments);
  g3->SetMarkerColor(4);
  g3->SetMarkerSize(0.5);
  g3->SetMarkerStyle(21);
  g3->SetTitle("Time adjustment at PMT positions due to cable lengths");
  g3->GetXaxis()->SetTitle("PMT Postion");
  g3->GetYaxis()->SetTitle("Time Adjustment/ns");
  g3->Draw("ap");
}





void InvestigateAdjustments(TH1F ** timeStampHisto,
			    double * pmtPos,
			    double * timeAdjustments,
			    double * flasherDistAdjustments,
			    double * cableLenAdjustments) {

  double unadjustedTimeStamps[channels];
  double hist_rms[channels];
  double adjustedTimeStamp[channels];
  double adjustedTimeStampDists[channels];
  double adjustedTimeStampCables[channels];

  UInt_t channelPos;
  
  // get unadjusted times
  for (int channel=0; channel<channels; channel++) {

    channelPos = pmtPos[channel] - 1;
    unadjustedTimeStamps[channel]    = timeStampHisto[channel]->GetMaximumBin();
    hist_rms[channel]                = timeStampHisto[channel]->GetRMS();
    
    adjustedTimeStamp[channel]       = unadjustedTimeStamps[channel] - flasherDistAdjustments[channelPos] - cableLenAdjustments[channelPos];

    adjustedTimeStampDists[channel]  = unadjustedTimeStamps[channel] - flasherDistAdjustments[channelPos];
    
    adjustedTimeStampCables[channel] = unadjustedTimeStamps[channel] - cableLenAdjustments[channelPos];
   
  }

  TCanvas *c4 = new TCanvas("c4", "c4", 1000, 500);
  
  TGraphErrors *gr1 = new TGraphErrors(channels, pmtPos, unadjustedTimeStamps, 0, hist_rms);
  gr1->SetName("gr1");
  gr1->SetTitle("Unadjusted Time Stamps");
  gr1->SetMarkerStyle(21);
  gr1->SetMarkerColor(1);
  //gr1->SetFillStyle(0);

  TGraphErrors *gr2 = new TGraphErrors(channels, pmtPos, adjustedTimeStamp, 0, hist_rms);
  gr2->SetName("gr2");
  gr2->SetTitle("Time Stamps Adjusted for Cables and Distances");
  gr2->SetMarkerStyle(22);
  gr2->SetMarkerColor(2);
  //gr2->SetFillStyle(0);

  TGraph *gr3 = new TGraph(channels, pmtPos, adjustedTimeStampDists);
  gr3->SetName("gr3");
  gr3->SetTitle("Time Stamps Adjusted for Distances");
  gr3->SetMarkerStyle(23);
  gr3->SetMarkerColor(3);
  //gr3->SetFillStyle(0);

  TGraph *gr4 = new TGraph(channels, pmtPos, adjustedTimeStampCables);
  gr4->SetName("gr4");
  gr4->SetTitle("Time Stamps Adjusted for Cables");
  gr4->SetMarkerStyle(24);
  gr4->SetMarkerColor(4);
  //gr3->SetFillStyle(0);

  
  TMultiGraph  *mg  = new TMultiGraph("mg", "mg");

  mg->SetTitle("Adjusted TimeStamps at PMT positions");
  mg->GetXaxis()->SetTitle("PMT Postion");
  mg->GetYaxis()->SetTitle("Time/ns");
  mg->GetYaxis()->SetLimits(140,200);
  
  mg->Add(gr1);
  mg->Add(gr2);
  mg->Add(gr3);
  mg->Add(gr4);

  gr3->Draw("ap");
  //mg->Add(gr4);
  mg->Draw("ap");
  c4->BuildLegend();
}

