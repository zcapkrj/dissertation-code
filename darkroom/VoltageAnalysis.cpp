#include "TotRatio.cpp"

void GetHv(Double_t *channelHv, TString pomId);
void PlotVoltageRatios(Double_t *totRatios, double *channelHv);

void VoltageAnalysis() {
  
  TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";
  TString pomId = "162666998";
  std::vector<TH1F*> totHistos;
  GetTots(fileName, & totHistos, pomId);

  Double_t channelHv[channels];
  GetHv(channelHv, pomId);
  // if the two lines above and below switched round totRatios[0] changes to 150 for some reason
  Double_t totRatios[channels];
  GetTotRatios(totHistos, totRatios);

  PlotVoltageRatios(totRatios, channelHv);
}

// need to find way of finding the pom number in convig file 
// function to read the hv of each pmt channel from config file 
void GetHv(Double_t *channelHv, TString pomId) {

  TString pomNum = GetPomIdNum(pomId);

  ifstream myfile ("datafiles/config.txt"); // open config file
  
  std::string line; // varible to hold each line 
  std::size_t found; // variable for pos of equals sign
  double hv;

  UInt_t channel = 0;
  while ( getline(myfile,line) ) { // read every line of file 
    // check if right pomid and  line is the hv argument
    if (line.find(("pom" + pomNum)) != std::string::npos && line.find("hv") != std::string::npos) {
	
      found = line.find("="); // find hv of equals sign
      hv = (std::stoi(line.substr(found+1))); // store hv in array, converted to an int

      hv = (hv * 800/254) + (700);

      std::cout << hv << std::endl;
      
      channelHv[channel] = (hv/14)*3; // find voltage on first dynode
      channel++;
    }
  }
  myfile.close();
}



void PlotVoltageRatios(Double_t *totRatios, Double_t *channelHv) {

  //std::cout << totRatios[0] << std::endl;
  
  // define canvas for the ratio of peaks data
  TCanvas *c = new TCanvas("c", "ratio", 1000, 600);
  TGraph *g = new TGraph(channels, channelHv, totRatios);


  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.08);
  gStyle->SetLabelSize(0.045, "XY");
  gStyle->SetTitleSize(0.05,"XY");
  gStyle->SetTitleOffset(1.,"X");
  gStyle->SetTitleOffset(0.7,"Y");

  //gStyle->SetOptFit(1111);
  g->SetMarkerColor(4);
  g->SetMarkerSize(0.7);
  g->SetMarkerStyle(21);
  g->SetTitle("Ratio of peaks' ToT vs Voltage across first dynode");
  g->GetXaxis()->SetTitle("Voltage across first dynode/V");
  g->GetYaxis()->SetTitle("Ratio of peaks' ToT");

  
  g->Fit("pol1");
  g->Draw("ap");
  //g->UseCurrentStyle();

  std::cout << "mean x: " << g->GetMean(1) << " mean y: " << g->GetMean(2) << std::endl;
}

