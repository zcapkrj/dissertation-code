#include "Plane.h"
#include "Plane.cpp"

#include <iostream>

int test_class() {

  Plane test(162689671);

  test.getTimeStamp();
  test.plotTimeStamp();
  
  // voltage plots
  /*
  test.get_init_file();
  test.getToT();
  test.getToTRatios();
  test.get_hv();
  test.plot_voltage_ratios();
  */

  /*
  test.getTimeStamp();

  test.get_pmt_pos();
  test.get_flasher_dist();
  test.get_cable_lengths();
  test.adjust_time_stamps();
  test.plot_adjustments();

  test.investigate_adjustments();
  */

  std::cout << "ran" << std::endl;
  return 0;
}
