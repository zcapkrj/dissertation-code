#ifndef DATE_H
#define DATE_H

#include <string>
#include <TROOT.h>

class Plane {

public:
  TString m_pomID;
  TTree * m_tree;
  string m_pom_num; // pom number in the convig file 

  // each type of plane a different class and sets this itself
  static const int channels = 30;
  const double c = 299792458; // speed of light m/s
  const double air_c = 299704644.54;
  const double cable_c = c * 0.64;
  const double s_To_ns = 1000000000;

  // array for histograms
  TH1D* tot_histos[channels];
  TH1D* time_stamp_histos[channels];
  double tot_peak[channels];
  double tot_ratio_peaks[channels];
  double pmt_hv[channels];
  double pmt_pos[channels];
  double flasher_dist[channels];
  double cable_lengths[channels];
  double time_adjustments[channels];

  double flasher_dist_pos[channels];
  double cable_lengths_pos[channels];
  
public:
  // constructor
  Plane(int pomID);

  void SetPlane(int pomID);
  void get_init_file();

  TString ToString(int num);

  // getting histos functions 
  void getToT();
  void getTimeStamp();
  void getToTRatios();
  void get_hv();
  void get_test();

  // plotting functions
  void plotToT();
  void plotTimeStamp();
  void plot_tot_ratios();
  void PlotTotRatioPlots();
  void plot_voltage_ratios();

  void get_pmt_pos();
  void get_flasher_dist();
  void get_cable_lengths();
  void adjust_time_stamps();
  void plot_adjustments();
  void adjusted_time_stamp();
  void plot_adjusted_time_stamp();

  void investigate_adjustments();

};

#endif

