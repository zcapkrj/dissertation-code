// rename all variables to cpp convention
// re orginise files into plotters and getters etc - go deeping, time adjustment functions and stuff together
// make file for stuff like get tot and that will run the functions for tot etc

#include "Plane.h"

#include <string>
#include <iostream>
#include <sstream>
#include <TROOT.h>
#include <cstring>

// Plane constructor
Plane::Plane(int pomID) {
  SetPlane(pomID);
}

// Plane member function constructor
void Plane::SetPlane(int pomID) {
  // convert to TString 
  m_pomID = ToString(pomID);
  
  // open file
  TFile *f = TFile::Open("planes/" + m_pomID + "/03POMs_NanobeaconA_6000mV.root");
  // error if file not open 
  if (f == 0) {std::cout << "Can't load file" << std::endl;}

  // set tree
  m_tree = (TTree*) f->Get("CLBOpt_tree"); // get the tree of all the data

  get_init_file();
}

void Plane::get_init_file() {
  
  ifstream config ("planes/" + m_pomID + "/config.txt"); // open config file
  
  std::string line; // varible to hold each line 
  std::size_t found; // variable for pos of equals sign
  
  if (config.is_open()) {
    // read every line of file 
    while ( getline(config,line) ) {
      // check if right pomid and  line is the hv argument
      if (line.find(m_pomID) != std::string::npos) {
	found = line.find("pom"); 
	m_pom_num = (line.substr(found+3,1));
      }
    }
    config.close();
  }

}

// convert number to TString object
TString Plane::ToString(int num) {
  ostringstream start;
  start << num;
  TString start1 = start.str();

  return start1;
}


// get all the tot histograms
void Plane::getToT() {
  // start loop and create all the histograms and add to canvas
  for (int i = 0; i<channels; i++) {
    TString numstr = ToString(i); // convert the integer 'i' to a TString
    TString histoname = "ToT of channel "+numstr; // create histograms name

    // declare the histogram
    tot_histos[i] = new TH1D(histoname, "ToT Frequency (Channel: "+numstr+")", 60, 0, 60);
    gStyle->SetTitleFontSize(0.1);
    // tot_histos[i]->GetYaxis()->SetTitle("Frequency");
    // tot_histos[i]->GetYaxis()->SetTitleSize(0.06);
    tot_histos[i]->GetYaxis()->SetLabelSize(0.06);
    //tot_histos[i]->GetXaxis()->SetTitle("ToT/ns");
    //tot_histos[i]->GetXaxis()->SetTitleSize(0.06);
    tot_histos[i]->GetXaxis()->SetLabelSize(0.06);
    tot_histos[i]->SetLineWidth(1);
    
    // fill the histogram
    m_tree->Project(histoname, "ToT", "PomId=="+ m_pomID + "&&Channel==" + numstr);
  }
}


// get ratios of tot peaks
void Plane::getToTRatios() {
  
  // histogram to find small peak that will be rewitten each time
  TH1F *smallPeak = new TH1F("smallPeak", "ToT", 10, 0, 10);

  // variables to calc peak ratio
  double a;
  double b;
  double ratio;
  
  // start loop to calculate ratios and add to canvas
  for (int i = 0; i<channels; i++) {

    TString numstr = ToString(i); // convert the integer 'i' to a TString

     // fill histogram for small peak
    m_tree->Project("smallPeak", "ToT", "PomId=="+ m_pomID + "&&Channel==" + numstr);
    
    // get the ratio of peaks
    a = tot_histos[i]->GetMaximumBin();
    b = smallPeak->GetMaximumBin();
    ratio = a/b;

    // store into class arrays
    tot_peak[i] = a;
    tot_ratio_peaks[i] = ratio;
  }  
}


// insert arguments for selecting the focusing, or adjust so focuses its self
// get all the time of event histograms
void Plane::getTimeStamp() {
  // variables to store data from root tree
  UInt_t timeStamp = 0;
  UInt_t pomID = 0;
  UChar_t channel = 0;

  // variables for the focusing histo
  int maxBin; int lower; int upper;

  // assign branchs to variables 
  m_tree->SetBranchAddress("TimeStamp_ns",& timeStamp);
  m_tree->SetBranchAddress("PomId",& pomID);
  m_tree->SetBranchAddress("Channel",& channel);

  // start loop and create all the histograms
  for (int channelSelection = 0; channelSelection < channels; channelSelection++) {

    TString num_str = ToString(channelSelection); // convert the integer 'channelSelection' to a TString
    TString histo_name = "time stamp of channel "+num_str; // create histograms name
    
    // declare the histogram
    time_stamp_histos[channelSelection] = new TH1D(histo_name, "TimeStamp of Channel "+num_str, 500, 0, 500);
    
    // loop through all the data and add the correct data to the hist
    for(int i=0; i<m_tree->GetEntries(); i++) {
      m_tree->GetEntry(i);

      if ((ToString(pomID) == m_pomID) & (channel == channelSelection)) {
	// take modules for pulse time
	time_stamp_histos[channelSelection]->Fill(timeStamp % 50000);
      }
    }

    // find the peak and the new upper and lower bounds
    maxBin = time_stamp_histos[channelSelection]->GetMaximumBin();

    lower = maxBin - 10;
    upper = maxBin + 10;

    // replace histo with focused one
    time_stamp_histos[channelSelection]->GetXaxis()->SetRange(lower,upper);
  }
}  



// need to find way of finding the pom number in convig file 
// function to read the hv of each pmt channel from config file 
void Plane::get_hv() {

  ifstream myfile ("planes/" + m_pomID + "/config.txt"); // open config file
  
  std::string line; // varible to hold each line 
  std::size_t found; // variable for pos of equals sign
  double hv;
  int channel = 0; // channel count
  
  if (myfile.is_open()) {
    // read every line of file 
    while ( getline(myfile,line) ) {
      // check if right pomid and  line is the hv argument
      if (line.find(("pom" + m_pom_num)) != std::string::npos &&
	  line.find("hv") != std::string::npos) {
	
	found = line.find("="); // find hv of equals sign
	hv = (std::stoi(line.substr(found+1))); // store hv in array, converted to an int


	std::cout << hv << std::endl;
	hv = (hv - 700) * (254/700); 

	pmt_hv[channel] = (hv/14)*3; // find voltage on first dynode
	channel++;
      }
    }
    myfile.close();
  }
}


// plot tot of each PMT and ratio of peaks
void Plane::plotToT() {
  
  // Define the split Canvas for ToT channels 
  TCanvas *tot_c = new TCanvas("tot_c", "canvas", 1000, 1000);
  gStyle->SetOptStat(0);

  tot_c->Divide(6,5,0.001,0.001,0); // divide canvas
  
  // start loop to add to canvas
  for (int i = 0; i<channels; i++) {

    TString numstr = ToString(i); // convert the integer 'i' to a TString

    tot_c->cd(i+1); // move to correct sub canvas
    
    tot_histos[i]->Draw(); // plot histo

    int maxBin = tot_histos[i]->GetMaximumBin();
    int maxBinSize = tot_histos[i]->GetBinContent(maxBin);
 
    tot_histos[i]->GetYaxis()->SetRangeUser(0, maxBinSize + 50);

    TLine *line = new TLine(31,0,31,maxBinSize + 50);
    line->SetLineWidth(1);
    line->Draw("same");
  }
}


// plot the time stamp data
void Plane::plotTimeStamp() {

  // create divided canvas
  TCanvas *time_stamp_c = new TCanvas("time_stamp_c", "canvas", 1000, 1000);
  gStyle->SetOptStat(0);
  time_stamp_c->Divide(6,5,0.001,0.001,0); // divide canvas

    for (int channel = 0; channel<channels; channel++) {

      gStyle->SetTitleFontSize(0.1);
      time_stamp_histos[channel]->GetXaxis()->SetTitle("TimeStamp/ns");
      time_stamp_histos[channel]->GetYaxis()->SetTitle("Frequency");

      time_stamp_histos[channel]->GetYaxis()->SetTitleSize(0.06);
      time_stamp_histos[channel]->GetYaxis()->SetLabelSize(0.05);
      time_stamp_histos[channel]->GetXaxis()->SetTitleSize(0.06);
      time_stamp_histos[channel]->GetXaxis()->SetLabelSize(0.05);
      time_stamp_histos[channel]->SetLineWidth(1);
      

      time_stamp_c->cd(channel + 1); // move to correct sub canvas 
      time_stamp_histos[channel]->Draw(); // plot histo
    }
}

void Plane::plot_tot_ratios() {

  // define canvas for the ratio of ToT peaks
  TCanvas *ratio_c = new TCanvas("ratio_c", "ratio", 600, 600);
  TH2F *ratVadcHisto = new TH2F("ratvadc", "Ratio vs ADC of peak", 6, 30, 36, 50, 5.0, 10.0);
  ratVadcHisto->SetTitle("Main Peak ToT vs ratio of peaks' ToT; Main peak ToT/ns; Ratio of Peaks' ToT");

  for (int i = 0; i<channels; i++) {
    ratVadcHisto->Fill(tot_peak[i],tot_ratio_peaks[i]);
  }

  //gPad->SetTicky(2);
  ratVadcHisto->GetYaxis()->SetLabelOffset(0.01);
  ratVadcHisto->Draw("COLZ");  
}


void Plane::PlotTotRatioPlots() {

  // define canvas for the ratio of ToT peaks
  TCanvas *ratio_c = new TCanvas("ratio_c", "ratio", 600, 600);
  ratio_c->Divide(1,2,0.01,0.01);

  // create histograms for ratios and ADC of peaks
  TH1F *ratHisto = new TH1F("ratty", "Ratio vs PMT number", 25, 0, 25);
  TH2F *ratVadcHisto = new TH2F("ratvadc", "Ratio vs ADC of peak", 100, 25,35,50,5.0,10.0);

  // setting the title and axis labels
  ratHisto->SetTitle("Ratio vs PMT number; PMT; Ratio of peaks");
  ratVadcHisto->SetTitle("ADC vs peak ratio; ADC; Ratio of Peaks");

  for (int i = 0; i<channels; i++) {
        // add ratio to ratio histogram
    ratHisto->SetBinContent(i,tot_ratio_peaks[i]);
    // not sure whats being added here
    ratVadcHisto->Fill(tot_peak[i],tot_ratio_peaks[i]);

  }

    // plot all the ratio histos
  ratio_c->cd(1);
  gPad->SetTickx(2);
  ratHisto->SetStats(111);
  ratHisto->Draw();

  ratio_c->cd(2);
  gPad->SetTicky(2);
  ratVadcHisto->GetYaxis()->SetLabelOffset(0.01);
  ratVadcHisto->Draw("box");  
}


void Plane::plot_voltage_ratios() {
    // define canvas for the ratio of peaks data
  TCanvas *c1 = new TCanvas("c1", "ratio", 600, 600);
  c1->Divide(1,2,0.01,0.01);
  
  c1->cd(1);
  TGraph *g1 = new TGraph(channels, pmt_hv, tot_ratio_peaks);
  g1->SetMarkerColor(4);
  g1->SetMarkerSize(0.5);
  g1->SetMarkerStyle(21);
  g1->SetTitle("Voltage vs Peak reatios");
  g1->GetXaxis()->SetTitle("Voltage of first dynode/V");
  g1->GetYaxis()->SetTitle("Ratio of peaks");
  g1->Draw("ap");

  c1->cd(2);
  TGraph *g2 = new TGraph(channels, pmt_hv, tot_peak);
  g2->SetMarkerColor(4);
  g2->SetMarkerSize(0.5);
  g2->SetMarkerStyle(21);
  g2->SetTitle("Voltage vs ADC");
  g2->GetXaxis()->SetTitle("Voltage of first dynode/V");
  g2->GetYaxis()->SetTitle("ADC");
  g2->Draw("ap");

}




void Plane::get_pmt_pos() {
  // assign each pmt to correct pos
  ifstream myfile ("planes/" + m_pomID + "/config.txt"); // open config file
  
  std::string line; // varible to hold each line 
  std::size_t found; // variable for pos of equals sign
  double pos;
  int channel = 0; // channel count
  
  if (myfile.is_open()) {
    // read every line of file 
    while ( getline(myfile,line) ) {
      // check if right pomid and  line is the hv argument
      if (line.find(("pom" + m_pom_num)) != std::string::npos &&
	  line.find("pos") != std::string::npos) {
	
	found = line.find("="); // find hv of equals sign
	pos = (std::stod(line.substr(found+1))); // convert to int

	// make sure we dont go over the max channels
	if (channel < channels) {
	  pmt_pos[channel] = pos; // store in pmt array
	  channel++;
	}
      }
    }
    myfile.close();
  }  
}


// read pos from flasher and cable length and calc the adjectment
void Plane::get_flasher_dist() {
  ifstream myfile ("planes/" + m_pomID + "/flasher_dist.txt"); // open config file
  
  std::string line; // varible to hold each line 
  double dist;
  int position = 0; // channel count
  
  if (myfile.is_open()) {
    // read every line of file 
    while ( getline(myfile,line) ) {
      dist = (std::stod(line.substr(line.length() - 3))); // dist is last 3 didgits of line 
      
      flasher_dist[position] = dist /100;
      position++;
    }
  }
  myfile.close();
}


void Plane::get_cable_lengths() {
  ifstream myfile ("planes/" + m_pomID + "/cable_lengths.txt");
  
  std::string line; // varible to hold each line 
  double length;
  int position = 0; // channel count
  
  if (myfile.is_open()) {
    // read every line of file 
    while ( getline(myfile,line) ) {
      length = (std::stod(line.substr(line.length() - 3))); // dist is last 3 didgits of line
      cable_lengths[position] = length / 100;
      position++;
    }
  }
  myfile.close();
}


// think get rid of this and just do within the plotting, or make 3 arrays for the 3 combos 
void Plane::adjust_time_stamps() {
  int channel_pos;
  double total_time; // hold time sum 

  // loop through all pmts
  for (int channel=0; channel<channels; channel++) {
    channel_pos = pmt_pos[channel] - 1;
    
    time_adjustments[channel] = ((flasher_dist[channel_pos] / air_c) + (cable_lengths[channel_pos] / cable_c)) * s_To_ns;
    
    flasher_dist_pos[channel] = (flasher_dist[channel_pos] / air_c) * s_To_ns;
    cable_lengths_pos[channel] = (cable_lengths[channel_pos] / cable_c) * s_To_ns;
  }
}


void Plane::plot_adjustments() {
  // define canvas for the ratio of peaks data
  TCanvas *c1 = new TCanvas("c1", "", 600, 600);
  TGraph *g1 = new TGraph(channels, pmt_pos, time_adjustments);
  g1->SetMarkerColor(4);
  g1->SetMarkerSize(0.5);
  g1->SetMarkerStyle(21);
  g1->SetTitle("Time adjustments at PMT positions cable lengths and distances");
  g1->GetXaxis()->SetTitle("PMT Postion");
  g1->GetYaxis()->SetTitle("Time Adjustment/ns");
  g1->Draw("ap");

  TCanvas *c2 = new TCanvas("c2", "", 600, 600);
  TGraph *g2 = new TGraph(channels, pmt_pos, flasher_dist_pos);
  g2->SetMarkerColor(4);
  g2->SetMarkerSize(0.5);
  g2->SetMarkerStyle(21);
  g2->SetTitle("Time adjustment at PMT positions flasher distances");
  g2->GetXaxis()->SetTitle("PMT Postion");
  g2->GetYaxis()->SetTitle("Time Adjustment/ns");
  g2->Draw("ap");

  TCanvas *c3 = new TCanvas("c3", "", 600, 600);
  TGraph *g3 = new TGraph(channels, pmt_pos, cable_lengths_pos);
  g3->SetMarkerColor(4);
  g3->SetMarkerSize(0.5);
  g3->SetMarkerStyle(21);
  g3->SetTitle("Time adjustment at PMT positions cable lengths");
  g3->GetXaxis()->SetTitle("PMT Postion");
  g3->GetYaxis()->SetTitle("Time Adjustment/ns");
  g3->Draw("ap");
}


void Plane::plot_adjusted_time_stamp() {

  double adjustedTimeStamp[channels];
  
  // get the peak values of timestamp histoms
  for (int channel=0; channel<channels; channel++) {
    adjustedTimeStamp[channel] = time_stamp_histos[channel]->GetMaximumBin() - time_adjustments[channel];
  }

  TCanvas *c3 = new TCanvas("c3", "", 600, 600);

  TGraph *g2 = new TGraph(channels, pmt_pos, adjustedTimeStamp);
  g2->SetMarkerColor(4);
  g2->SetMarkerSize(0.5);
  g2->SetMarkerStyle(21);
  g2->SetTitle("Adjusted TimeStamps at PMT positions");
  g2->GetXaxis()->SetTitle("PMT Postion");
  g2->GetYaxis()->SetTitle("Time/ns");
  g2->Draw("ap");
}


void Plane::investigate_adjustments() {
  int channel_pos;
  double unadjustedTimeStamps[channels];
  double adjustedTimeStamp[channels];
  double adjustedTimeStampDists[channels];
  double adjustedTimeStampCables[channels];
  double hist_rms[channels];
  
  // get the peak values of timestamp histoms
  for (int channel=0; channel<channels; channel++) {

    channel_pos = pmt_pos[channel] - 1;
    std::cout << channel_pos << std::endl;
    
    double originalTime = time_stamp_histos[channel]->GetMaximumBin();
    hist_rms[channel] = time_stamp_histos[channel]->GetRMS();

    unadjustedTimeStamps[channel] = originalTime;
    
    adjustedTimeStamp[channel] = originalTime - time_adjustments[channel];

    adjustedTimeStampDists[channel] = originalTime - (flasher_dist[channel_pos] / air_c) * s_To_ns;

    adjustedTimeStampCables[channel] = originalTime - (cable_lengths[channel_pos] / cable_c) * s_To_ns;

    std::cout << unadjustedTimeStamps[channel]  << std::endl;
    std::cout << adjustedTimeStamp[channel]  << std::endl;
    std::cout << adjustedTimeStampDists[channel]  << std::endl;
    std::cout << adjustedTimeStampCables[channel]  << std::endl;
    std::cout << ""  << std::endl;
    //std::cout << (flasher_dist[channel_pos] / air_c) << std::endl;
  }

  TCanvas *c4 = new TCanvas("c4", "c4", 1000, 500);

  TGraphErrors *gr1 = new TGraphErrors(channels, pmt_pos, unadjustedTimeStamps, 0, hist_rms);
  gr1->SetName("gr1");
  gr1->SetTitle("Unadjusted Time Stamps");
  gr1->SetMarkerStyle(21);
  gr1->SetMarkerColor(1);
  //gr1->SetFillStyle(0);

  TGraphErrors *gr2 = new TGraphErrors(channels, pmt_pos, adjustedTimeStamp, 0, hist_rms);
  gr2->SetName("gr2");
  gr2->SetTitle("Time Stamps Adjusted for Cables and Distances");
  gr2->SetMarkerStyle(22);
  gr2->SetMarkerColor(2);
  //gr2->SetFillStyle(0);

  TGraph *gr3 = new TGraph(channels, pmt_pos, adjustedTimeStampDists);
  gr3->SetName("gr3");
  gr3->SetTitle("Time Stamps Adjusted for Distances");
  gr3->SetMarkerStyle(23);
  gr3->SetMarkerColor(3);
  //gr3->SetFillStyle(0);

  TGraph *gr4 = new TGraph(channels, pmt_pos, adjustedTimeStampCables);
  gr4->SetName("gr4");
  gr4->SetTitle("Time Stamps Adjusted for Cables");
  gr4->SetMarkerStyle(24);
  gr4->SetMarkerColor(4);
  //gr3->SetFillStyle(0);
  
  TMultiGraph  *mg  = new TMultiGraph("mg", "mg");

  mg->SetTitle("Adjusted TimeStamps at PMT positions");
  mg->GetXaxis()->SetTitle("PMT Postion");
  mg->GetYaxis()->SetTitle("Time/ns");
  mg->GetYaxis()->SetLimits(140,200);
  
  mg->Add(gr1);
  mg->Add(gr2);
  mg->Add(gr3);
  mg->Add(gr4);

  gr3->Draw("ap");
  //mg->Add(gr4);
  mg->Draw("ap");
  c4->BuildLegend();
}
