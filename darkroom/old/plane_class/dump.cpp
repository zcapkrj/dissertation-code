Plane test("03POMs_NanobeaconA_5800mV.root", 162625384)

// function converts an int to TString
int tot_channels() {

  // open the file
  TFile *f = TFile::Open("20190828_000_confFile_20190828_095840_03POMs_NanobeaconA_5800mV_acceptanceTest_nano.root");
  if (f == 0) 
    {
      // if we cannot open the file, print an error message and return immediatly
      std::cout << "Can't load file" << std::endl;
    }

  // get the tree of all the data
  TTree* tree = (TTree*) f->Get("CLBOpt_tree");

  // Define the split Canvas for ToT channels 
  TCanvas *c = new TCanvas("c", "canvas", 1000, 1000);
  gStyle->SetOptStat(111111);
  c->Divide(6,5,0.01,0.01,0); // divide canvas

  // define canvas for the ratio of peaks data
  TCanvas *c1 = new TCanvas("c1", "ratio", 600, 600);
  c1->Divide(1,2,0.01,0.01);
  
  // find way of getting this from file
  int numOfHistos = 30;
  
  // set and convert pomID to string
  int pomID = 162666998;
  TString pomIDstr = ToString(pomID);
  
  // array of histograms that will be plotted
  TH1D* histos[numOfHistos];
  
  // histogram to find small peak that will be rewitten each time
  TH1F *smallPeak = new TH1F("smallPeak", "ToT", 10, 0, 10);

  // variables to work peak ratio
  double a;
  double b;
  double ratio;
  
  // create histograms for ratios and ADC of peaks
  TH1F *ratHisto = new TH1F("ratty", "Ratio vs PMT number", 25, 0, 25);
  TH2F *ratVadcHisto = new TH2F("ratvadc", "Ratio vs ADC of peak", 100, 25,35,50,5.0,10.0);
  // setting the title and axis labels
  ratVadcHisto->SetTitle("ADC vs peak ratio; ADC; Ratio of Peaks");
  
  // start loop and create all the histograms and add to canvas
  for (int i = 1; i<numOfHistos + 1; i++) {
    
    // convert the integert 'i' to a TString
    TString numstr = ToString(i);

    // create histograms name
    TString histoname = "channel "+numstr;

    // declare the histogram
    histos[i] = new TH1D(histoname, "ToT Channel "+numstr, 100, 0, 100);

    // fill the histogram
    tree->Project(histoname, "ToT", "PomId=="+pomIDstr+"&&Channel=="+numstr);
    
    c->cd(i); // move to correct sub canvas 
    histos[i]->Draw(); // plot histo

    // fill histogram for small peak
    tree->Project("smallPeak", "ToT", "PomId=="+pomIDstr+"&&Channel=="+numstr);

    // get the ratio of peaks
    a =  histos[i]->GetMaximumBin();
    b =  smallPeak->GetMaximumBin();
    ratio = a/b;

    // add ratio to ratio histogram
    ratHisto->SetBinContent(i,ratio);
    // not sure whats being added here
    ratVadcHisto->Fill(a,ratio);
  }

  c1->cd(1);
  gPad->SetTickx(2);
  ratHisto->SetStats(111);
  ratHisto->Draw();

  c1->cd(2);
  gPad->SetTicky(2);
  ratVadcHisto->GetYaxis()->SetLabelOffset(0.01);
  ratVadcHisto->Draw("box");
  
  return 0;
}

