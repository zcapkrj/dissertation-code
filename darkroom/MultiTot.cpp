#include "CoreFuns.cpp"

void GetTots(TString fileName, std::vector<TH1F*> *totHistos, TString pomId);
void PlotTots(std::vector<TH1F*> totHistos);


void MultiTot() {

  
  TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";
  std::vector<TH1F*> totHistos;
  
  GetTots(fileName, & totHistos, "162666998");
  PlotTots(totHistos);
}


// get tot plots of a plane
void GetTots(TString fileName, std::vector<TH1F*> *totHistos, TString pomId) {

    // open the file
  TFile *f = TFile::Open(fileName);
  TTree* tree = (TTree*) f->Get("CLBOpt_tree");   // get the tree of all the data

  TH1F *histoHolder;
 
  // start loop and create all the histograms and add to canvas
  for (int i = 0; i<channels; i++) {

    TString channelStr = ToString(i); // convert the integer 'i' to a TString
    TString histoname = "ToT of channel "+ channelStr; // create histograms name

    // declare the histogram
    histoHolder = new TH1F(histoname, "ToT Frequency (Channel: " +channelStr+ ")", 60, 0, 60);
    tree->Project(histoname, "ToT", "PomId=="+ pomId + "&&Channel==" +channelStr);
    
    totHistos->push_back(histoHolder);
  }
}


// plot tot plots 
void PlotTots(std::vector<TH1F*> totHistos) {
  
  // Define the split Canvas for ToT channels 
  TCanvas *c = new TCanvas("c", "canvas", 1000, 1000);
  gStyle->SetOptStat(0);

  c->Divide(6,5,0.00001,0.00001,0); // divide canvas ( set for 30 channels)
  
  // start loop to add to canvas
  for (int i = 0; i<channels; i++) {

    gStyle->SetOptStat(0);
    gStyle->SetTitleFontSize(.08);
    gStyle->SetLabelSize(0.05, "XY");
    gStyle->SetTitleSize(0.07,"XY");
    gStyle->SetTitleOffset(0.7,"X");
    gStyle->SetTitleOffset(1,"Y");

    c->cd(i+1); // move to correct sub canvas    
    totHistos[i]->Draw(); // plot histo
    totHistos[i]->UseCurrentStyle();


    // adjust canvas for line to fit
    int maxBin = totHistos[i]->GetMaximumBin();
    int maxBinSize = totHistos[i]->GetBinContent(maxBin);
    totHistos[i]->GetYaxis()->SetRangeUser(0, maxBinSize + 50);

    // add line at 31ns
    TLine *line = new TLine(31,0,31,maxBinSize + 50);
    line->SetLineWidth(1);
    line->Draw("same");
  }
}
