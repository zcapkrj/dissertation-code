#include "CoreFuns.cpp"

// need to add auto adjustment of max freq so fits better

void PlotSingleToT(TString fileName, TString pomId, TString channel);

void SingleTot() {
  
  TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";

  PlotSingleToT(fileName, "162666998", "0");
}

void PlotSingleToT(TString fileName, TString pomId, TString channel) {

  // open the file
  TFile *f = TFile::Open(fileName);
  TTree* tree = (TTree*) f->Get("CLBOpt_tree");   // get the tree of all the data
  
  TH1F *h1 = new TH1F("h1", "ToT (Plane: " +pomId+ " and Channel: " +channel+ ")", 60, 0, 60);

  tree->Project("h1", "ToT", "PomId==" +pomId+ "&&Channel==" +channel); // select plane and channel

  TCanvas *c = new TCanvas("c", "canvas", 800, 800); // define the Canvas
  c->SetLeftMargin(0.15); // adjust for label being cut off

  h1->GetYaxis()->SetTitle("Frequency");
  h1->GetXaxis()->SetTitle("ToT/ns");
  h1->SetLineWidth(4);

  gStyle->SetOptStat(1);
  gStyle->SetTitleFontSize(.09);
  gStyle->SetLabelSize(0.04, "XY");
  gStyle->SetTitleSize(0.04,"XY");
  gStyle->SetTitleOffset(1,"X");
  gStyle->SetTitleOffset(1.4,"Y");

  
  h1->Draw();
  h1->UseCurrentStyle();
  h1->GetYaxis()->SetRangeUser(0, 450);

  // add line at 31ns
  TLine *line = new TLine(31,0,31,450);
  line->SetLineWidth(2);
  line->Draw("same");
}
