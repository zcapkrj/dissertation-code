#include "MultiTot.cpp"

void GetTotRatios(std::vector<TH1F*> totHistos, Double_t* totRatios);
void TotRatioPlots(std::vector<TH1F*> totHistos, Double_t* totRatios);

void TotRatio() {
  
  TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";

  std::vector<TH1F*> totHistos;
  GetTots(fileName, & totHistos, "162666998");

  Double_t totRatios[channels];
  GetTotRatios(totHistos, totRatios);
  TotRatioPlots(totHistos, totRatios);
  
}

// get ratios of tot peaks
void GetTotRatios(std::vector<TH1F*> totHistos, Double_t* totRatios) {
  
    // variables to calc peak ratio
  double a; double b; double ratio;
  
  // loop over every histo
  for (int i = 0; i<channels; i++) {

    // get the ratio of peaks
    a = totHistos[i]->GetMaximumBin();
    
    totHistos[i]->GetXaxis()->SetRange(1,10); // focus on small peak (sometime bin 1 has large freq)
    b = totHistos[i]->GetMaximumBin();
    totHistos[i]->GetXaxis()->SetRange(0,60); // unfocus
    
    if (b != 0) {
      ratio = a/b;
      totRatios[i] = ratio;

      //std::cout << i << " " << totRatios[i] << std::endl;	    
    }
    else {
      totRatios[i] = 999; // indicates error in location small peak
    }
  }  
}


void TotRatioPlots(std::vector<TH1F*> totHistos, Double_t* totRatios) {

  TCanvas *c = new TCanvas("c", "ratio", 1000, 600);


  // create histograms for ratios and ADC of peaks
  TH2F *ratioHisto = new TH2F("ratioHisto", "Ratio of Peaks' ToT vs ToT of Main Peak", 7, 29,36,50,5.0,9.0);
  ratioHisto->SetTitle("Ratio of Peaks' ToT vs ToT of Main Peak; ToT of Main Peak/ns;  Ratio of peaks' ToT");

  for (int i = 0; i<channels; i++) {

    ratioHisto->Fill(totHistos[i]->GetMaximumBin(), totRatios[i]);
    std::cout << totHistos[i]->GetMaximumBin() << " " << totRatios[i] << std::endl;
  }

  gStyle->SetOptStat(0);
  gStyle->SetTitleFontSize(.1);
  gStyle->SetLabelSize(0.04, "XY");
  gStyle->SetTitleSize(0.045,"XY");
  gStyle->SetTitleOffset(0.9,"X");
  gStyle->SetTitleOffset(1,"Y");
  //gPad->SetTicky(2);

  ratioHisto->Draw("COLZ");
  ratioHisto->UseCurrentStyle();
  ratioHisto->GetYaxis()->SetRangeUser(0, 8.5);
}
