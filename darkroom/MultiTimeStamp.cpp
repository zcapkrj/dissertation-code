#include "SingleTimeStamp.cpp"

void GetTimeStamps(TString fileName, TH1F ** timestampHistos, TString pomId);
void PlotTimeStamps(TH1F ** timeStampHistos);

void MultiTimeStamp() {
  
   TString fileName = "datafiles/03POMs_NanobeaconA_6000mV.root";

   TH1F* timeStampHistos[channels];  

   GetTimeStamps(fileName, timeStampHistos, "162666998");
   PlotTimeStamps(timeStampHistos);

}




// get tot plots of a plane
void GetTimeStamps(TString fileName, TH1F ** timeStampHistos, TString pomId) {

  for (int i = 0; i < channels; i++) {

    TString iStr = ToString(i);
    TString histoName = "TimeStamp of Channel " + iStr; 
    
    timeStampHistos[i] = new TH1F(histoName, histoName, 500, 0, 500);
    
    GetSingleTimeStamp(fileName, timeStampHistos[i], pomId, iStr);
    std::cout << i << std::endl;
  }
}



// plot tot plots 
void PlotTimeStamps(TH1F ** timeStampHistos) {
  
  // Define the split Canvas for ToT channels 
  TCanvas *c = new TCanvas("c", "canvas", 1000, 1000);

  c->Divide(6,5,0.000001,0.000001,0); // divide canvas ( set for 30 channels)
  
  // start loop to add to canvas
  for (int i = 0; i<channels; i++) {

      int maxBin; int lower; int upper; // vars to focus plot on peak
      maxBin = timeStampHistos[i]->GetMaximumBin();
      lower = maxBin - 10;
      upper = maxBin + 10;
      timeStampHistos[i]->GetXaxis()->SetRange(lower,upper);

      gStyle->SetOptStat(0);
      gStyle->SetTitleFontSize(.08);
      gStyle->SetLabelSize(0.05, "XY");
      gStyle->SetTitleSize(0.07,"XY");
      gStyle->SetTitleOffset(0.7,"X");
      gStyle->SetTitleOffset(1,"Y");

      timeStampHistos[i]->SetLineWidth(4);  
      timeStampHistos[i]->GetYaxis()->SetTitle("");
      timeStampHistos[i]->GetXaxis()->SetTitle("");


      c->cd(i+1); // move to correct sub canvas    
      timeStampHistos[i]->Draw(); // plot histo
      timeStampHistos[i]->UseCurrentStyle();
      //timeStampHistos[i]->GetXaxis()->SetRangeUser(160, 200);
  }
}



